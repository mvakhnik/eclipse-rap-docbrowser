package vmk.docbrowser.dao;

import java.util.Collection;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.osgi.service.log.LogService;

import vmk.docbrowser.common.dao.CopyException;
import vmk.docbrowser.common.dao.IDocumentNodeDAO;
import vmk.docbrowser.common.entity.Content;
import vmk.docbrowser.common.entity.Document;
import vmk.docbrowser.common.entity.DocumentDirectory;
import vmk.docbrowser.common.entity.DocumentNode;
import vmk.docbrowser.common.provider.DocumentOperationResult;
import vmk.docbrowser.common.provider.EntityManagerProvider;
import aQute.bnd.annotation.component.Activate;
import aQute.bnd.annotation.component.Component;
import aQute.bnd.annotation.component.Reference;

/**
 * The IDocumentNodeDAO JPA implementation.
 * 
 * @author Maksim Vakhnik
 *
 */
@Component
public class DocumentDAO implements IDocumentNodeDAO {

	private EntityManager entityManager;
	private EntityManagerProvider entityManagerProvider;

	private LogService log;
	
	@Reference(optional = true, dynamic = true)
	private void setLog(LogService log) {
		this.log = log;
	}
	
	@SuppressWarnings("unused")
	private void unsetLog(LogService log) {
		this.log = null;
	}
	
	/**
	 * Write log info message.
	 * @param message
	 */
	public void log (String message) {
		log(LogService.LOG_INFO, message);
	}
	
	/**
	 * If LogService is available - log to LogService, else log to System.out(err)
	 * @param logLevel
	 * @param message
	 */
	private  void log(int logLevel, String message) {
		if (log != null) {
			log.log(logLevel, message);
		} else {
			if (logLevel == LogService.LOG_ERROR 
					|| logLevel == LogService.LOG_WARNING) {
				System.err.println(message);
			} else {
				System.out.println(message);
			}
		}
	}
	
	@Activate
	public void init() {
		log("IDocumentNodeDAO started:" + this);
		log("IDocumentNodeDAO: entityManager:" + entityManager);
	}

	DocumentDAO getInstance() {
		return this;
	}

	@Override
	public DocumentNode create(DocumentNode doc) {
		EntityManager entityManager = getEntityManager();
		EntityTransaction et = getEntityManager().getTransaction();
		et.begin();
		try {
			entityManager.persist(doc);
			et.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return doc;
	}

	@Override
	public DocumentNode read(String nodeId) {
		EntityManager em = getEntityManager();
		DocumentNode result = em.find(DocumentNode.class,
				nodeId);
		if (result != null) {
			em.detach(result);
		}

		return result;
	}

	@Override
	public DocumentNode read(DocumentNode node) {
		return read(node.getId());
	}

	/*
	 * Return true if exist.
	 */
	public boolean isExist(DocumentNode node) {
		if (node == null) {
			return false;
		}
//		DocumentNode newNode = new Document(node.getName());
		
		return isExist(node.getId());
	}

	@Override
	public boolean isExist(String docId) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getReference(DocumentNode.class, docId);
			return true;
		}  catch(EntityNotFoundException ex) {
			return false;
		}
	}

	private boolean isExistContent(String id) {
		EntityManager entityManager = getEntityManager();
		
		try {
			entityManager.getReference(Content.class, id);
			return true;
		}  catch(EntityNotFoundException ex) {
			return false;
		}
	}
	@Override
	public DocumentNode update(DocumentNode doc) {
		if (doc == null) {
			return null;
		}
		EntityManager entityManager = getEntityManager();
		EntityTransaction et = entityManager.getTransaction();
		et.begin();
		try {
			entityManager.persist(doc);
			et.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return doc;
	}

	@Override
	public void remove(DocumentNode node) {
		remove(node.getId());
	}

	
	@Override
	public void remove(String nodeId) {
		EntityManager entityManager = getEntityManager();
		EntityTransaction et = entityManager.getTransaction();
		DocumentNode removed = entityManager.find(DocumentNode.class, nodeId);
		if (removed != null && removed.isDirectory()) {
			Collection<DocumentNode> children = getChildren(removed);
			for (DocumentNode child : children) {
				remove(child);
			}
		}
		
		if (!removed.isDirectory()) {
			removeContent(removed.getId());
		}
		et.begin();
		entityManager.remove(removed);
		et.commit();
	}

	/*
	 * Remove content if exist
	 */
	private void removeContent (String  contentId) {
		if (isExistContent(contentId)) {
			EntityManager em = getEntityManager();
			
			EntityTransaction et = em.getTransaction();
			et.begin();
			Content removed = em.find(Content.class, contentId);
			em.remove(removed);
			et.commit();
		}
	}
	
	@Override
	public Collection<DocumentNode> getChildren(DocumentNode parent) {
		EntityManager entityManager = getEntityManager();
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<DocumentNode> cQuery = builder
				.createQuery(DocumentNode.class);
		Root<DocumentNode> nodes = cQuery.from(DocumentNode.class);
		cQuery.select(nodes).where(builder.equal(nodes.get("parent"), parent));
		Query query = entityManager.createQuery(cQuery);
		@SuppressWarnings("unchecked")
		Collection<DocumentNode> result = query.getResultList();
//		entityManager.clear();
		return result;
	}

	private Content findContent(String docId) {
		EntityManager entityManager = getEntityManager();
		return entityManager.find(Content.class, docId);
	}

	private Content createContent(String docId) {
		EntityManager entityManager = getEntityManager();
		DocumentNode doc = this.read(docId);
		Content content = new Content((Document) doc);
		EntityTransaction et = entityManager.getTransaction();
		et.begin();
		entityManager.persist(content);
		et.commit();
		return content;
	}

	@Override
	public byte[] readContent(Document doc, int start, int length) {
		if (doc == null) {
			throw new NullPointerException(
					"Content cannot be read from null document");
		}
		Content content = findContent(doc.getId());
		if (content != null) {
			return content.read(start, length);
		}
		return null;
	}

	@Override
	public void writeContent(Document doc, int start, byte[] data) {
		if (doc == null) {
			throw new NullPointerException(
					"Content cannot be writted to null document");
		}
		EntityManager entityManager = getEntityManager();
		Content content = findContent(doc.getId());
		// if content not found - create it.
		if (content == null) {
			content = createContent(doc.getId());
		}
		content.write(start, data);
		EntityTransaction et = entityManager.getTransaction();
		et.begin();
		entityManager.persist(content);
		// em.persist(content.getBlobData());
		et.commit();
	}

	@Override
	public void writeContent(Document doc, byte[] data) {
		if (doc == null) {
			throw new NullPointerException(
					"Content cannot be writted to null document");
		}
		Content content = findContent(doc.getId());
		// if content not found - create it.
		if (content == null) {
			content = createContent(doc.getId());
		}
		content.setData(data);
		EntityManager entityManager = getEntityManager();
		EntityTransaction et = entityManager.getTransaction();
		et.begin();
		entityManager.persist(content);
		et.commit();

	}

	@Override
	public byte[] readContent(Document doc) {
		if (doc == null) {
			throw new NullPointerException(
					"Content cannot be read from null document");
		}
		Content content = findContent(doc.getId());
		if (content != null) {
			log("get data:" + new String(content.getData()));
			return content.getData();
		}
		return null;
	}

	private EntityManager getEntityManager() {
		if (this.entityManager == null || !this.entityManager.isOpen()) {
			entityManager = getEntityManagerProvider().getEntityManager();
		}
		return entityManager;
	}

	private EntityManagerProvider getEntityManagerProvider() {
		return entityManagerProvider;
	}

	@Reference(dynamic = true)
	private void setEntityManagerProvider(
			EntityManagerProvider entityManagerProvider) {
		System.out
				.println("set entityManagerProvider:" + entityManagerProvider);
		this.entityManagerProvider = entityManagerProvider;
	}

	@SuppressWarnings("unused")
	private void unsetEntityManagerProvider(
			EntityManagerProvider entityManagerProvider) {
		log("unset entityManagerProvider:"
				+ entityManagerProvider);
		this.entityManagerProvider = entityManagerProvider;
	}

	@Override
	public DocumentOperationResult copy(DocumentNode source, DocumentDirectory target)
			throws CopyException {
		DocumentNode copyResult = source;
		//check paste possibility
		DocumentOperationResult isCanPaste = isCanPaste(source, target);
		if (isCanPaste != DocumentOperationResult.OK) {
			throw new CopyException(isCanPaste);
		} else {
			EntityTransaction et = entityManager.getTransaction();
			et.begin();
			copyResult = copyRecursively(source, target);
			et.commit();
		}
		return DocumentOperationResult.OK.setResult(copyResult);
		
	}

	private DocumentNode copyRecursively(DocumentNode source, DocumentDirectory target) throws CopyException {
		DocumentNode copy = source;
		//if file - copy file, copy content
		if (!source.isDirectory()) {
			//copy content
			Content content  = findContent(source.getId());
			//create copy of document
			copy = source.copy();
			copy.setParent(target);
			if (content != null) {
				Content copyContent = content.createCopy((Document) copy);
				entityManager.persist(copyContent);
			}
			
			// if content not found - create it.
			entityManager.persist(copy);
		} else {
			copy = source.copy();
			copy.setParent(target);
			
			Collection<DocumentNode> children = getChildren(source);
			entityManager.persist(copy);
			for (DocumentNode node : children) {
				copyRecursively(node, (DocumentDirectory) copy);
			}
		}
		
		return copy;
		//if dir - copy dir, after copy children
	}

	/**
	 * Check copy or move possibility.
	 * @param inserted
	 * @param target
	 * @return {@code DocumentOperationResult}, 
	 * if can - return {@code DocumentOperationResult.OK} with null data.
	 */
	private DocumentOperationResult isCanPaste(DocumentNode inserted, DocumentNode target) {
		
		if (Objects.equals(inserted, target)) {
			return DocumentOperationResult.ERROR_SOURCE_IS_TARGET;
		}
		if (target != null && ! target.isDirectory()) {
			return DocumentOperationResult.ERROR_TARGET_IS_NOT_DIRECTORY;
		}
//		if (! inserted.isDirectory()) {
//			return DocumentOperationResult.OK;
//		}
		//if src is parent (parent of parent) of target
		if (haveChildren(inserted, target)) {
			return DocumentOperationResult.ERROR_TARGET_IS_CHILD_OF_SOURCE;
		}
		
		return DocumentOperationResult.OK;
	}
	
	/*
	 * 
	 * @return true if inserted is parent of target in some knee
	 */
	private boolean haveChildren(DocumentNode inserted, DocumentNode target) {
		DocumentNode parentTarget = target;
		while (parentTarget != null) {
			if (Objects.equals(inserted, parentTarget)) {
				return true;
			}
			parentTarget = parentTarget.getParent();
		}
		return false;
	}

	private void createContentCopy(Content original, Document newOwner) {
		Content contentCopy = original.createCopy(newOwner);
		EntityManager entityManager = getEntityManager();
		EntityTransaction et = entityManager.getTransaction();
		
		et.begin();
		entityManager.persist(contentCopy);
		et.commit();
	}
	@Override
	public DocumentOperationResult rename(DocumentNode document, String name) {
		//create renamed 
		DocumentNode renamed = document.copy();
		renamed.setParent(document.getParent());
		renamed.setName(name);
		if (isExist(renamed)) {
			return  DocumentOperationResult.ERROR_DOCUMENT_EXIST;
		}
		DocumentNode created = create(renamed);
		if (created != null) {
			if (created.isDirectory()) {
				Collection<DocumentNode> children = getChildren(document);
				for (DocumentNode documentNode : children) {
					DocumentOperationResult result;
					try {
						copy(documentNode, (DocumentDirectory) created);
					} catch (CopyException e) {
						remove(created);
						e.printStackTrace();
						return e.getResult();
					}
				}
			} else {
				Content contentOriginal = findContent(document.getId());
				if (contentOriginal != null) {
					createContentCopy(contentOriginal, (Document) created);
				}
			}
			remove(document);
			return DocumentOperationResult.OK.setResult(created);
		} else {
			return DocumentOperationResult.ERROR.setMessage("Document cannot be created");
		}
	}
	
}
