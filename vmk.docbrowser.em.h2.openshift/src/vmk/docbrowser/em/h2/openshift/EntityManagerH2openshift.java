package vmk.docbrowser.em.h2.openshift;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.osgi.service.jpa.EntityManagerFactoryBuilder;
import org.osgi.service.log.LogService;

import vmk.docbrowser.common.provider.EntityManagerProvider;
import aQute.bnd.annotation.component.Activate;
import aQute.bnd.annotation.component.Component;
import aQute.bnd.annotation.component.Reference;

/**
 * Create EntityManager for openshift with dynamic ip.
 * 
 * @author Maksim Vakhnik
 *
 */
@Component
public class EntityManagerH2openshift implements EntityManagerProvider {
	
	private static final String OPENSHIFT_INTERNAL_IP = "OPENSHIFT_DIY_IP";
	private static final String DB_PORT = ":15081";
	private static final String DATABASE = "app-root/data/test-db";

	private EntityManager em;
	@Activate
	private void activate() {
		log("EntityManagerH2openshift activate:" + this);
	}

	public void setEmf(EntityManagerFactory emf) {
		this.em = emf.createEntityManager();
	}

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}

	
	
	
	private Properties getProperties() {
		Properties props = new Properties();
		String ip = System.getenv(OPENSHIFT_INTERNAL_IP);

		if (ip == null || ip.isEmpty()) {
			ip = "localhost";
		}
		log("openshift ip:" + ip);
		props.put("javax.persistence.jdbc.url", "jdbc:h2:tcp://" + ip
				+ DB_PORT
				+ "/~/" + DATABASE);
		return props;
	}
//
//	private EntityManagerFactoryBuilder getEmfb() {
//		return emfb;
//	}

	@Reference(dynamic = true)
	private void setEmfb(EntityManagerFactoryBuilder emfb) {
		if (emfb != null) {
			createEntityManager(emfb);
		}

	}
	
	@SuppressWarnings("unused")
	private void unsetEmfb(EntityManagerFactoryBuilder emfb) {
		if (em != null) {
			em.close();
		}
	}

	private void createEntityManager(EntityManagerFactoryBuilder emfb) {
		Properties props = getProperties();
		@SuppressWarnings({ "unchecked", "rawtypes" })
		Map<String, Object> map = new HashMap(props);
		
		EntityManagerFactory emf = emfb.createEntityManagerFactory(map);
		setEmf(emf);
		
	}

	
	private LogService log;
	
	@Reference(optional = true, dynamic = true)
	private void setLog(LogService log) {
		this.log = log;
	}
	
	@SuppressWarnings("unused")
	private void unsetLog(LogService log) {
		this.log = null;
	}
	
	/**
	 * Write log info message.
	 * @param message
	 */
	public void log (String message) {
		log(LogService.LOG_INFO, message);
	}
	
	/**
	 * If LogService is available - log to LogService, else log to System.out(err)
	 * @param logLevel
	 * @param message
	 */
	private  void log(int logLevel, String message) {
		if (log != null) {
			log.log(logLevel, message);
		} else {
			if (logLevel == LogService.LOG_ERROR 
					|| logLevel == LogService.LOG_WARNING) {
				System.err.println(message);
			} else {
				System.out.println(message);
			}
		}
	}

}
