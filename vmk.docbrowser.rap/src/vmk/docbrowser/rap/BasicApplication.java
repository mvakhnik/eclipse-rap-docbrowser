package vmk.docbrowser.rap;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.rap.rwt.application.Application;
import org.eclipse.rap.rwt.application.ApplicationConfiguration;
import org.eclipse.rap.rwt.client.WebClient;

import aQute.bnd.annotation.component.Component;

/**
 * Starts the RAP application. Activated through OSGi Declarative Service.
 * @author Maksim Vakhnik
 *
 */
@Component
public class BasicApplication implements ApplicationConfiguration {
	
    public void configure(Application application) {
        Map<String, String> properties = new HashMap<String, String>();
        properties.put(WebClient.PAGE_TITLE, "Docbrowser");
        application.addEntryPoint("/", DocbrowserEntryPoint.class, properties);
       
    }

}
