package vmk.docbrowser.rap.parts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import vmk.docbrowser.common.entity.DocumentNode;
import vmk.docbrowser.rap.DocumentBuffer;
import vmk.docbrowser.rap.EventBusUISession;
import vmk.docbrowser.rap.actions.ActionCopySelection;
import vmk.docbrowser.rap.actions.ActionCutSelection;
import vmk.docbrowser.rap.actions.ActionPasteInDocumentNode;
import vmk.docbrowser.rap.actions.ActionSaveActiveEditor;
import vmk.docbrowser.rap.actions.ActionStartDeleteDocumentDialog;
import vmk.docbrowser.rap.actions.ActionStartDocumentRenaming;
import vmk.docbrowser.rap.actions.ActionStartNewDirectoryDialog;
import vmk.docbrowser.rap.actions.ActionStartNewDocumentDialog;
import vmk.docbrowser.rap.events.EventChangeActiveEditor;
import vmk.docbrowser.rap.listener.ListenerDocumentNodeSelectionEvent;
import vmk.docbrowser.rap.listener.ListenerDocumentNodeSelectionEvent.DocumentNodeSelectionListener;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;

/**
 * UI part: menu bar part.
 * @author Maksim Vakhnik
 *
 */
public class MenuBar extends Composite {

	private ToolBar toolBar;
	private ToolBar toolBarEditor;
	private ToolItem tltmNewDir;
	private ToolItem tltmNewDoc;
	private ToolItem tiDelete;
	private ToolItem tiSave;
	private ToolItem tiCopy;
	private ToolItem tiPaste;
	private ToolItem tiCut;
	private ToolItem tiRename;
	
	private static final long serialVersionUID = 201501011437L;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public MenuBar(Composite parent, int style) {
		super(parent, style);
		
		initUi();
		initEventReceing();
	}

	private void initEventReceing() {
		EventBusUISession.register(this);
		
	}

	/**
	 * When editor is activated and dirty - enable save button.
	 * @param event
	 */
	@Subscribe
	@AllowConcurrentEvents
	public void onActivateEditor(EventChangeActiveEditor event) {
		if (event != null) {
			Editor editor = ManagerDocumentEditor.getInstance().getActiveEditor();
			if (editor != null) {
				tiSave.setEnabled(editor.isDirty());
			} else {
				tiSave.setEnabled(false);
			}
			
		} else {
			tiSave.setEnabled(false);
		}
	}
	
	
	private void initUi() {
		setLayout(new RowLayout(SWT.HORIZONTAL));
		Group groupDocuments = new Group (this, SWT.SHADOW_ETCHED_IN);
		groupDocuments.setLayout (new FillLayout ());
		groupDocuments.setText ("Documents operations");
		toolBar = new ToolBar(groupDocuments, SWT.FLAT | SWT.RIGHT);
		addItemNewDir(toolBar);
		addItemNewDoc(toolBar);
		addItemRename(toolBar);
		addItemCopy(toolBar);
		addItemCut(toolBar);
		addItemPaste(toolBar);
		addItemDelete(toolBar);
		Group groupEditor = new Group (this, SWT.SHADOW_ETCHED_IN);
		groupEditor.setLayout (new FillLayout ());
		groupEditor.setText ("Editor operations");
		toolBarEditor = new ToolBar(groupEditor, SWT.FLAT | SWT.RIGHT);
		addItemSave(toolBarEditor);
		
	}


	private void addItemRename(ToolBar toolBar) {
		tiRename = new ToolItem(toolBar, SWT.PUSH);
		tiRename.setText("Rename");
		tiRename.addSelectionListener(new SelectionAdapter() {
			private static final long serialVersionUID = 201501031820L;

			@Override
			public void widgetSelected(SelectionEvent e) {
				ActionStartDocumentRenaming.run();
			}
		});
		tiRename.setEnabled(false);
		tiRename.setEnabled(false);
		ListenerDocumentNodeSelectionEvent
				.addDocumentSelectionLlistener(new DocumentNodeSelectionListener() {
					@Override
					public void onSelect(DocumentNode selection) {
						tiRename.setEnabled(isNotRoot(selection));
					}
				});
	}

	/*
	 * Return true if selection is not root.
	 */
	private boolean isNotRoot(DocumentNode document) {
		return document != null && ! document.isRoot();
	}
	

	private void addItemSave(ToolBar toolBar) {
		tiSave = new ToolItem(toolBar, SWT.PUSH);
		tiSave.setText("Save");
		tiSave.addSelectionListener(new SelectionAdapter() {
			private static final long serialVersionUID = 201501031820L;

			@Override
			public void widgetSelected(SelectionEvent e) {
				ActionSaveActiveEditor.run();
			}
		});
		tiSave.setEnabled(false);
		
	}


	
	private void addItemDelete(ToolBar toolBar) {

		tiDelete = new ToolItem(toolBar, SWT.PUSH);
		tiDelete.setText("Delete");
		tiDelete.addSelectionListener(new SelectionAdapter() {
			private static final long serialVersionUID = 201501031820L;

			@Override
			public void widgetSelected(SelectionEvent e) {
				ActionStartDeleteDocumentDialog.run();
			}
		});
		tiDelete.setEnabled(false);
		
		ListenerDocumentNodeSelectionEvent
		.addDocumentSelectionLlistener(new DocumentNodeSelectionListener() {
			@Override
			public void onSelect(DocumentNode selection) {
				tiDelete.setEnabled(isNotRoot(selection));
			}
		});

	}

	private void addItemNewDoc(ToolBar toolBar) {
		tltmNewDoc = new ToolItem(toolBar, SWT.PUSH);
		tltmNewDoc.setText("New document");
		tltmNewDoc.addSelectionListener(new SelectionAdapter() {
			private static final long serialVersionUID = 201501011437L;

			@Override
			public void widgetSelected(SelectionEvent e) {
				ActionStartNewDocumentDialog.run();
			}
		});
		
		ListenerDocumentNodeSelectionEvent
		.addDocumentSelectionLlistener(new DocumentNodeSelectionListener() {
			@Override
			public void onSelect(DocumentNode selection) {
				tltmNewDoc.setEnabled(! isFile(selection));
				tltmNewDir.setEnabled(! isFile(selection));
			}
		});

	}

	protected boolean isFile(DocumentNode selection) {
		return (selection != null && selection.isFile());
	}


	private void addItemCopy(ToolBar toolBar) {
		tiCopy = new ToolItem(toolBar, SWT.PUSH);
		tiCopy.setText("Copy");
		tiCopy.addSelectionListener(new SelectionAdapter() {
			private static final long serialVersionUID = 201501031820L;

			@Override
			public void widgetSelected(SelectionEvent e) {
				ActionCopySelection.run();
			}
		});
		tiCopy.setEnabled(false);
		ListenerDocumentNodeSelectionEvent
		.addDocumentSelectionLlistener(new DocumentNodeSelectionListener() {
			@Override
			public void onSelect(DocumentNode selection) {
				tiCopy.setEnabled(isNotRoot(selection));
			}
		});
	}

	private void addItemCut(ToolBar toolBar) {
		tiCut = new ToolItem(toolBar, SWT.PUSH);
		tiCut.setText("Cut");
		tiCut.addSelectionListener(new SelectionAdapter() {
			private static final long serialVersionUID = 201501031820L;

			@Override
			public void widgetSelected(SelectionEvent e) {
				ActionCutSelection.run();
			}
		});

		tiCut.setEnabled(false);
		ListenerDocumentNodeSelectionEvent
				.addDocumentSelectionLlistener(new DocumentNodeSelectionListener() {
					@Override
					public void onSelect(DocumentNode selection) {
						tiCut.setEnabled(isNotRoot(selection));
					}
				});

	}

	private void addItemPaste(ToolBar toolBar) {
		tiPaste = new ToolItem(toolBar, SWT.PUSH);
		tiPaste.setText("Paste");
		tiPaste.addSelectionListener(new SelectionAdapter() {
			private static final long serialVersionUID = 201501031820L;

			@Override
			public void widgetSelected(SelectionEvent e) {
				ActionPasteInDocumentNode.run();
			}
		});
		tiPaste.setEnabled(false);
		ListenerDocumentNodeSelectionEvent
				.addDocumentSelectionLlistener(new DocumentNodeSelectionListener() {
					@Override
					public void onSelect(DocumentNode selection) {
						DocumentNode src = DocumentBuffer.getInstance().getPasted();
						if (src != null  && selection.isDirectory()) {
							tiPaste.setEnabled(true);
						} else {
							tiPaste.setEnabled(false);
						}

					}
				});

	}

	private void addItemNewDir(ToolBar toolBar) {

		tltmNewDir = new ToolItem(toolBar, SWT.PUSH);
		tltmNewDir.addSelectionListener(new SelectionAdapter() {
			private static final long serialVersionUID = 201501011437L;

			@Override
			public void widgetSelected(SelectionEvent e) {
				ActionStartNewDirectoryDialog.run();
			}
		});
		tltmNewDir.setText("New directory");
		
		ListenerDocumentNodeSelectionEvent
		.addDocumentSelectionLlistener(new DocumentNodeSelectionListener() {
			@Override
			public void onSelect(DocumentNode selection) {
				tltmNewDir.setEnabled(! isFile(selection));
			}
		});
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
