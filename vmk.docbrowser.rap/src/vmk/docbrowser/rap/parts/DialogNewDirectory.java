package vmk.docbrowser.rap.parts;

import java.util.ResourceBundle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import vmk.docbrowser.common.entity.DocumentDirectory;
import vmk.docbrowser.common.entity.DocumentNode;
import vmk.docbrowser.rap.UIContextDialog;
import vmk.docbrowser.rap.actions.ActionCreateNewDirectory;
import vmk.docbrowser.rap.listener.ListenerDocumentNodeSelectionEvent;
import vmk.docbrowser.rap.listener.ListenerDocumentNodeSelectionEvent.DocumentNodeSelectionListener;

/**
 * UI part: dialog of document's creation.
 * @author Maksim Vakhnik
 *
 */
public class DialogNewDirectory extends UIContextDialog{
	private static final ResourceBundle BUNDLE = ResourceBundle
			.getBundle("vmk.docbrowser.rap.messages"); //$NON-NLS-1$

	private static final long serialVersionUID = 201412311928L;
	private Text textParentDirectory;
	private Text textNewDirectory;
	private Button btnCreate;
	private Button btnCancel;
	private Label lblMessage;


	/**
	 * @wbp.nonvisual location=42,198
	 */

	/**
	 * Create the shell.
	 * 
	 * @param display
	 */
	public DialogNewDirectory(Shell shell, int style)  {
		super(DialogNewDirectory.class, shell, style);
//		super( shell, style);

		Label lblNewLabel = new Label(this, SWT.NONE);
		lblNewLabel.setText(BUNDLE.getString("labelParentDirectory")); //$NON-NLS-1$
		FormData fd_lblNewLabel = new FormData();
		fd_lblNewLabel.top = new FormAttachment(0, 16);
		// fd_lblNewLabel.bottom = new FormAttachment(100, -229);
		fd_lblNewLabel.left = new FormAttachment(0, 10);
		fd_lblNewLabel.right = new FormAttachment(100, -10);
		lblNewLabel.setLayoutData(fd_lblNewLabel);

		lblNewLabel.setToolTipText(BUNDLE.getString("toolTipParentDirectory"));
		
		textParentDirectory = new Text(this, SWT.BORDER);
		
		textParentDirectory.setEditable(false);
//		textParentDirectory.setToolTipText(BUNDLE.getString("toolTipParentDirectory"));
		textParentDirectory.setToolTipText(BUNDLE.getString("toolTipParentDirectory"));
		
		FormData fd_textParentDirectory = new FormData();
		fd_textParentDirectory.top = new FormAttachment(lblNewLabel, 6);
		fd_textParentDirectory.left = new FormAttachment(0, 10);
		fd_textParentDirectory.right = new FormAttachment(100, -10);
		textParentDirectory.setLayoutData(fd_textParentDirectory);

		Label lblNewDirectory = new Label(this, SWT.NONE);

		lblNewDirectory.setText(BUNDLE.getString("labelNewDirectory")); //$NON-NLS-1$
		FormData fd_lblNewDirectory = new FormData();
		fd_lblNewDirectory.top = new FormAttachment(textParentDirectory, 20);
		// fd_lblNewDirectory.bottom = new FormAttachment(textNewDirectory, -6);
		fd_lblNewDirectory.left = new FormAttachment(lblNewLabel, 0, SWT.LEFT);
		lblNewDirectory.setLayoutData(fd_lblNewDirectory);

		textNewDirectory = new Text(this, SWT.BORDER);

		FormData fd_text = new FormData();
		fd_text.top = new FormAttachment(lblNewDirectory, 6);
		fd_text.left = new FormAttachment(0, 10);
		textNewDirectory.setLayoutData(fd_text);

		btnCreate = new Button(this, SWT.NONE);
		fd_text.right = new FormAttachment(btnCreate, 0, SWT.RIGHT);
		btnCreate.setText(BUNDLE.getString("buttonCreate")); //$NON-NLS-1$
		FormData fd_btnNewButton = new FormData();
		fd_btnNewButton.right = new FormAttachment(100, -10);
		fd_btnNewButton.bottom = new FormAttachment(100, -10);
		btnCreate.setLayoutData(fd_btnNewButton);

		btnCancel = new Button(this, SWT.NONE);
		btnCancel.setText(BUNDLE.getString("buttonCancel")); //$NON-NLS-1$
		FormData fd_btnNewButton_1 = new FormData();
		fd_btnNewButton_1.top = new FormAttachment(btnCreate, 0, SWT.TOP);
		fd_btnNewButton_1.right = new FormAttachment(btnCreate, -6);
		btnCancel.setLayoutData(fd_btnNewButton_1);

		lblMessage = new Label(this, SWT.NONE);
		lblMessage.setText(""); //$NON-NLS-1$
		FormData fd_lblMessages = new FormData();
		fd_lblMessages.top = new FormAttachment(textNewDirectory, 18);
		fd_lblMessages.left = new FormAttachment(0, 10);
		fd_lblMessages.right = new FormAttachment(btnCreate, 0, SWT.RIGHT);
		fd_lblMessages.bottom = new FormAttachment(btnCreate, -2);

		lblMessage.setLayoutData(fd_lblMessages);
		databinding();
		createContents();
		initListeners();
//		initInstance();
	}

	private void databinding() {
		ListenerDocumentNodeSelectionEvent
			.addDocumentSelectionLlistener(new DocumentNodeSelectionListener() {
			
			@Override
			public void onSelect(DocumentNode selection) {
				String id = selection.getPath();
				id = id == null ? "" : id;
				DialogNewDirectory.this.textParentDirectory.setText(id);
				
			}
		});
	}

	private void initListeners() {

		this.btnCreate.addMouseListener(new MouseListener() {
			private static final long serialVersionUID = 201501012306L;
			
			@Override
			public void mouseUp(MouseEvent e) {
				final DocumentNode  node = ListenerDocumentNodeSelectionEvent.getLastSelection();
				if (node == null || node.isDirectory()) {
					ActionCreateNewDirectory.create((DocumentDirectory) node, getNewDirName());
				}
				
			}

			@Override
			public void mouseDown(MouseEvent e) {

			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {

			}
		});

		this.btnCancel.addMouseListener(new MouseListener() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 201501061529L;

			@Override
			public void mouseUp(MouseEvent e) {
				DialogNewDirectory.this.close();
			}

			@Override
			public void mouseDown(MouseEvent e) {

			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {

			}
		});
	}


	private String getNewDirName() {
		return textNewDirectory.getText();
	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {
		setText(BUNDLE.getString("titleDialogNewDirectory")); //$NON-NLS-1$
//		initSizePosition();

	}



	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}


}
