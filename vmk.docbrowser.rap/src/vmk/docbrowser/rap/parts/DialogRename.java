/**
 * 
 */
package vmk.docbrowser.rap.parts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.widgets.Shell;

import vmk.docbrowser.common.entity.DocumentNode;
import vmk.docbrowser.rap.UIContextDialog;
import vmk.docbrowser.rap.actions.ActionRenameDocument;
import vmk.docbrowser.rap.listener.ListenerDocumentNodeSelectionEvent;
import vmk.docbrowser.rap.listener.ListenerDocumentNodeSelectionEvent.DocumentNodeSelectionListener;

/**
 * UI part: dialog of document's renaming.
 * 
 * @author Maksim Vakhnik
 *
 */
public class DialogRename extends UIContextDialog {

	private static final long serialVersionUID = 201501042206L;
	private String txtCaption = "Rename document";
	private String txtLabel0 = "Select renamed document:";

	private String txtLabel1 = "Input new name:";

	private String txtBtnOk = "Rename";

	private String txtBtnCancel = "Cancel";

	private CompositeDialog content;
	private DocumentNode selectedDocument;

	public DialogRename(Shell shell, int style) {
		super(DialogRename.class, shell, style);
		setContent();
		addListeners();
	}

	private void addListeners() {

		ListenerDocumentNodeSelectionEvent
				.addDocumentSelectionLlistener(new DocumentNodeSelectionListener() {

					@Override
					public void onSelect(DocumentNode selected) {
						if (selected != null && ! selected.isRoot()) {
							setSelectedDocument(selected);
							String id = selected.getName();
							id = id == null ? "" : id;
							content.getText0().setText(id);

						}

					}
				});
		content.getBtnOk().addMouseListener(new MouseListener() {

			private static final long serialVersionUID = 201501051044L;

			@Override
			public void mouseUp(MouseEvent e) {
				ActionRenameDocument.rename(getSelectedDocument(), content
						.getText1().getText());

			}

			@Override
			public void mouseDown(MouseEvent e) {

			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {

			}
		});

		content.getBtnCancel().addMouseListener(new MouseListener() {
			private static final long serialVersionUID = 201501051044L;

			@Override
			public void mouseUp(MouseEvent e) {
				close();
			}

			@Override
			public void mouseDown(MouseEvent e) {
				// do nothing

			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				// do nothing

			}
		});
	}

	public void setContent() {
		content = new CompositeDialog(this, SWT.NONE);
		this.setText(txtCaption);
		content.getLabel0().setText(txtLabel0);
		content.getLabel1().setText(txtLabel1);
		content.getBtnOk().setText(txtBtnOk);
		content.getBtnCancel().setText(txtBtnCancel);

	}

	private DocumentNode getSelectedDocument() {
		return selectedDocument;
	}

	private void setSelectedDocument(DocumentNode selectedDocument) {
		this.selectedDocument = selectedDocument;
	}

}
