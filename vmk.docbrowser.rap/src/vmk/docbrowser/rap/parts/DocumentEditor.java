/**
 * 
 */
package vmk.docbrowser.rap.parts;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import vmk.docbrowser.common.entity.Document;
import vmk.docbrowser.common.provider.DocumentOperationResult;
import vmk.docbrowser.common.provider.Documents;
import vmk.docbrowser.rap.Context;
import vmk.docbrowser.rap.EventBusUISession;
import vmk.docbrowser.rap.events.EventMessage;
import vmk.docbrowser.rap.events.EventMessage.Level;

import com.google.common.base.Objects;

/**
 * Simple document editor. Edits document content as simple text. Writes/reads in UTF-8.
 * 
 * @author Maksim Vakhnik
 *
 */
public class DocumentEditor implements Editor {

	private CompositeDocumentEditor editor;
	private final Document document;
	private String savedContent;
	private Documents documents;
	private final static String CHARSET = "UTF-8";
	
	public DocumentEditor(Document document) {
		this.document = document;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see vmk.docbrowser.rap.Editor#getName()
	 */
	@Override
	public String getName() {
		return document != null ? document.getPath() : "empty";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see vmk.docbrowser.rap.Editor#getControl()
	 */
	@Override
	public Control build(Composite parent) {
		editor = new CompositeDocumentEditor(parent, SWT.NONE);
		loadText();
		return editor;
	}

	private void loadText() {
		try {
			documents = (Documents) Context.getDocuments();
			DocumentOperationResult result = documents.readFrom(document);
			if (result == DocumentOperationResult.OK) {
				savedContent = new String(resultToText(result));
				editor.getTextEditor().setText(savedContent);
				
			} else {
				EventBusUISession.post(new EventMessage(Level.ERROR, result.getMessage()));
			}
		} catch (Exception e) {
			EventBusUISession.post(new EventMessage(Level.ERROR, 
					"Can`t read document data"));
		}

	}
	
	@Override
	public void save() {
		if (isDirty()) {
			String text = this.getEditedText();
			DocumentOperationResult result;
			try {
				result = documents.writeTo(document, text.getBytes(CHARSET));
				if (result == DocumentOperationResult.OK) {
					this.savedContent = this.editor.getTextEditor().getText();
					this.editor.getTextEditor().setText(this.savedContent);
					EventBusUISession.post(new EventMessage(Level.INFO, result.getMessage()));
				} else {
					EventBusUISession.post(new EventMessage(Level.ERROR, result.getMessage()));
				}
			} catch (UnsupportedEncodingException e) {
				EventBusUISession.post(new EventMessage(Level.ERROR, 
						"Text not saved: UTF-8 not supported by server"));
			}
			

		}
	}

	
	private String resultToText(DocumentOperationResult result) {
		byte[] data = (byte[]) result.getResult();
		String text = "";
		if (data != null) {
			text = new String(data, Charset.forName(CHARSET));
		}
		return text;
	}

	private String getEditedText() {
		return editor.getTextEditor().getText();
	}
	
	/**
	 * Indicate existing not saved changes in document.
	 * @return true if have not saved changes.
	 */
	public boolean isDirty() {
		String savedText = savedContent;
		String currentText = editor.getTextEditor().getText();
		boolean textNotSame = ! Objects.equal(savedText, currentText);
		return textNotSame;
	}

	@Override
	public void addModifyListener(ModifyListener listener) {
		editor.getTextEditor().addModifyListener(listener);
		
	}

	@Override
	public void removeModifyListener(ModifyListener listener) {
		editor.getTextEditor().removeModifyListener(listener);
		
	}

	@Override
	public Document getEditableData() {
		return this.document;
	}

	@Override
	public void dispose() {
		editor.getTextEditor().dispose();
		
	}
	
	

}
