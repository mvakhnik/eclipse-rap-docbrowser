package vmk.docbrowser.rap.parts;


import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ViewForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;

/**
 * UI part: main window of application.
 * @author Maksim Vakhnik
 *
 */
public class MainWindow extends Composite {


	private static final long serialVersionUID = 201501061527L;
	private ViewForm viewForm;
	private Composite compositeContent;
	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public MainWindow(Composite parent, int style) {
		super(parent, style);
		setLayout(new FormLayout());
		
		

		
//		Label label = new Label(composite_1, SWT.NONE);
//		label.setText("label");

		
		Composite composite = new Composite(this, SWT.NONE);
		FormData fd_composite = new FormData();
		fd_composite.top = new FormAttachment(0, 3);
		fd_composite.bottom = new FormAttachment(100, 0);
		fd_composite.right = new FormAttachment(100, -3);
		
		fd_composite.left = new FormAttachment(0, 3);
		composite.setLayoutData(fd_composite);
		composite.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		viewForm = new ViewForm(composite, SWT.NONE);
		
		Composite compositeTopLeft = new Composite(viewForm, SWT.NONE);
		viewForm.setTopLeft(compositeTopLeft);
		
		Composite compositeTopCenter = new Composite(viewForm, SWT.NONE);
		viewForm.setTopCenter(compositeTopCenter);
		
		Composite compositeTopRight = new Composite(viewForm, SWT.NONE);
		viewForm.setTopRight(compositeTopRight);
		
		compositeContent = new Composite(viewForm, SWT.NONE);
		viewForm.setContent(compositeContent);
		

	}

	public Composite getContent() {
		return compositeContent;
	}
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public ViewForm getViewForm() {
		return viewForm;
	}


}
