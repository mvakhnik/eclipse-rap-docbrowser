package vmk.docbrowser.rap.parts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

/**
 * Content part of application.
 * @author Maksim Vakhnik
 *
 */
public class ContentPart extends Composite {

	private static final long serialVersionUID = 201501051738L;
	
	private Composite leftPart;
	private Composite rightPart;
	private SashForm sashForm;
	/**
	 * Create the composite with {@code SashForm}.
	 * SashPart and its left, right panel of form accessed by getters.
	 * @param parent
	 * @param style
	 */
	public ContentPart(Composite parent, int style) {
		super(parent, style);
		setLayout(new FillLayout(SWT.HORIZONTAL));
		
		sashForm = new SashForm(this, SWT.BORDER | SWT.SMOOTH);
		leftPart = new Composite(sashForm, SWT.NONE);
		leftPart.setLayout(new FillLayout(SWT.VERTICAL));
		
		rightPart = new Composite(sashForm, SWT.NONE);
		rightPart.setLayout(new FillLayout(SWT.VERTICAL));
		sashForm.setWeights(new int[] { 153, 294 });
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public Composite getLeftPart() {
		return leftPart;
	}

	public Composite getRightPart() {
		return rightPart;
	}

	public SashForm getSashForm() {
		return sashForm;
	}
	
	
}
