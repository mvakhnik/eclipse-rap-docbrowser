package vmk.docbrowser.rap.parts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

/**
 * Creates the composite part of the simple document editor.
 * @author Maksim Vakhnik
 *
 */
public class CompositeDocumentEditor extends Composite {

	private static final long serialVersionUID = 201501051706L;
	private Text textEditor;
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public CompositeDocumentEditor(Composite parent, int style) {
		super(parent, style);
		setLayout(new FillLayout(SWT.HORIZONTAL));
		
		textEditor = new Text(this, SWT.BORDER | SWT.WRAP | SWT.H_SCROLL | SWT.V_SCROLL | SWT.CANCEL | SWT.MULTI);
		textEditor.setText("Edit text");
		
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	Text getTextEditor() {
		return textEditor;
	}

}
