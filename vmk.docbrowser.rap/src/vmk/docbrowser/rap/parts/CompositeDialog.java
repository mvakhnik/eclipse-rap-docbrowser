package vmk.docbrowser.rap.parts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * Base class for dialogs with two labels and two texts.
 * @author Maksim Vakhnik
 *
 */
public class CompositeDialog extends Composite {

	private static final long serialVersionUID = 201501042306L;
	
	private Label label0;
	private Text text0;
	
	private Label label1;
	private Text text1;
	private Button btnOk;
	private Button btnCancel;
	/**
	 * Creates the composite.
	 * @param parent
	 * @param style
	 */
	public CompositeDialog(Composite parent, int style)  {
		super(parent, style);
		this.setLayout(new FillLayout());
		build(parent);
	}

	public void build(Composite parent) {
		label0 = new Label(parent, SWT.NONE);
		label0.setText("Label 0");
		FormData fd_lblNewLabel = new FormData();
		fd_lblNewLabel.top = new FormAttachment(0, 16);
		// fd_lblNewLabel.bottom = new FormAttachment(100, -229);
		fd_lblNewLabel.left = new FormAttachment(0, 10);
		fd_lblNewLabel.right = new FormAttachment(100, -10);
		label0.setLayoutData(fd_lblNewLabel);

		
		text0 = new Text(parent, SWT.BORDER);
		
		text0.setEditable(false);
//		textParentDirectory.setToolTipText(BUNDLE.getString("toolTipParentDirectory"));
		
		FormData fd_textParentDirectory = new FormData();
		fd_textParentDirectory.top = new FormAttachment(label0, 6);
		fd_textParentDirectory.left = new FormAttachment(0, 10);
		fd_textParentDirectory.right = new FormAttachment(100, -10);
		text0.setLayoutData(fd_textParentDirectory);

		label1 = new Label(parent, SWT.NONE);

		label1.setText("labelNewDirectory"); //$NON-NLS-1$
		FormData fd_lblNewDirectory = new FormData();
		fd_lblNewDirectory.top = new FormAttachment(text0, 20);
		// fd_lblNewDirectory.bottom = new FormAttachment(textNewDirectory, -6);
		fd_lblNewDirectory.left = new FormAttachment(label0, 0, SWT.LEFT);
		label1.setLayoutData(fd_lblNewDirectory);

		text1 = new Text(parent, SWT.BORDER);

		FormData fd_text = new FormData();
		fd_text.top = new FormAttachment(label1, 6);
		fd_text.left = new FormAttachment(0, 10);
		text1.setLayoutData(fd_text);

		FormData fdMessager = new FormData();
		fdMessager.top = new FormAttachment(text1, 0, SWT.BOTTOM);
		fdMessager.right = new FormAttachment(100, -10);
		fdMessager.left = new FormAttachment(0, 10);
		
		btnOk = new Button(parent, SWT.NONE);
		
		fdMessager.bottom = new FormAttachment(btnOk, 0, SWT.TOP);
		fd_text.right = new FormAttachment(btnOk, 0, SWT.RIGHT);
		
		btnOk.setText("buttonCreate"); //$NON-NLS-1$
		FormData fd_btnNewButton = new FormData();
		fd_btnNewButton.right = new FormAttachment(100, -10);
		fd_btnNewButton.bottom = new FormAttachment(100, -10);
		btnOk.setLayoutData(fd_btnNewButton);

		btnCancel = new Button(parent, SWT.NONE);
		btnCancel.setText("buttonCancel"); //$NON-NLS-1$
		FormData fd_btnNewButton_1 = new FormData();
		fd_btnNewButton_1.top = new FormAttachment(btnOk, 0, SWT.TOP);
		fd_btnNewButton_1.right = new FormAttachment(btnOk, -6);
		btnCancel.setLayoutData(fd_btnNewButton_1);
	}
	
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	protected Label getLabel0() {
		return label0;
	}

	protected CompositeDialog setLabel0(Label label0) {
		this.label0 = label0;
		return this;
	}

	protected Text getText0() {
		return text0;
	}

	protected CompositeDialog setText0(Text text0) {
		this.text0 = text0;
		return this;
	}

	protected Label getLabel1() {
		return label1;
	}

	protected CompositeDialog setLabel1(Label label1) {
		this.label1 = label1;
		return this;
	}

	protected Text getText1() {
		return text1;
	}

	protected CompositeDialog setText1(Text text1) {
		this.text1 = text1;
		return this;
	}

	protected Button getBtnOk() {
		return btnOk;
	}

	protected CompositeDialog setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
		return this;
	}

	protected Button getBtnCancel() {
		return btnCancel;
	}

	protected CompositeDialog setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
		return this;
	}

}
