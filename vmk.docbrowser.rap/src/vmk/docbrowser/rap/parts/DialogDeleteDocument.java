package vmk.docbrowser.rap.parts;

import org.eclipse.rap.rwt.widgets.DialogCallback;
import org.eclipse.rap.rwt.widgets.DialogUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import vmk.docbrowser.common.entity.DocumentNode;
import vmk.docbrowser.rap.EventBusUISession;
import vmk.docbrowser.rap.UIContextDialog;
import vmk.docbrowser.rap.docviewer.ElementProvider;
import vmk.docbrowser.rap.events.EventMessage;
import vmk.docbrowser.rap.events.EventMessage.Level;
import vmk.docbrowser.rap.listener.ListenerDocumentNodeSelectionEvent;
import vmk.docbrowser.rap.listener.ListenerDocumentNodeSelectionEvent.DocumentNodeSelectionListener;

/**
 * UI part: dialog of document's deleting.
 * @author Maksim Vakhnik
 *
 */
public class DialogDeleteDocument extends UIContextDialog {
	private static final long serialVersionUID = 201501031835L;

	private Text textSelectDocument;
	private Button btnAskConfirmation;
	private Button btnDelete;
	private Button btnCancel;

	private String txtBtnConfirm = "Ask confirmation for not empty";
	private String txtLblDelete = "Select document for deleting";
	private String txtBtnDelete = "Delete";
	private String txtBtnCancel = "Cancel";
	private String txtCaption = "Delete document";
	private String toolTip = "Select document or directory in document browser.";
	
	/**
	 * Creates the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public DialogDeleteDocument(Shell parent, int style) {
		super(DialogDeleteDocument.class, parent, style);
		createUI();

		databinding();
		initListeners();
	}

	private void createUI() {
		setLayout(new FormLayout());

		Label lblDeleteDocument = new Label(this, SWT.NONE);
		FormData fd_lblDeleteDocument = new FormData();
		fd_lblDeleteDocument.top = new FormAttachment(0, 10);
		fd_lblDeleteDocument.left = new FormAttachment(0, 10);
		lblDeleteDocument.setLayoutData(fd_lblDeleteDocument);
		lblDeleteDocument.setText(txtLblDelete);

		lblDeleteDocument.setToolTipText(toolTip);

		textSelectDocument = new Text(this, SWT.BORDER);
		textSelectDocument.setEditable(false);
		FormData fd_text = new FormData();
		fd_text.right = new FormAttachment(100, -10);
		fd_text.top = new FormAttachment(lblDeleteDocument, 6);
		fd_text.left = new FormAttachment(lblDeleteDocument, 0, SWT.LEFT);
		textSelectDocument.setLayoutData(fd_text);
		textSelectDocument.setToolTipText(toolTip);

		btnAskConfirmation = new Button(this, SWT.CHECK);
		FormData fd_btnAskConfirmation = new FormData();
		fd_btnAskConfirmation.bottom = new FormAttachment(100, -65);
		fd_btnAskConfirmation.left = new FormAttachment(0, 10);
		btnAskConfirmation.setLayoutData(fd_btnAskConfirmation);
		btnAskConfirmation.setText(txtBtnConfirm);

		btnDelete = new Button(this, SWT.NONE);
		FormData fd_btnDelete = new FormData();
		fd_btnDelete.bottom = new FormAttachment(100, -10);
		fd_btnDelete.right = new FormAttachment(100, -10);
		btnDelete.setLayoutData(fd_btnDelete);
		btnDelete.setText(txtBtnDelete);

		btnCancel = new Button(this, SWT.NONE);
		FormData fd_btnCancel = new FormData();
		fd_btnCancel.bottom = new FormAttachment(btnDelete, 0, SWT.BOTTOM);
		fd_btnCancel.right = new FormAttachment(btnDelete, -6);
		btnCancel.setLayoutData(fd_btnCancel);
		btnCancel.setText(txtBtnCancel);

		this.setText(txtCaption);

	}

	private void initListeners() {
		this.btnDelete.addMouseListener(new MouseListener() {
			private static final long serialVersionUID = 201501012306L;

			@Override
			public void mouseUp(MouseEvent e) {
				DocumentNode selected = ListenerDocumentNodeSelectionEvent
						.getLastSelection();
				if (selected != null) {
					deleteWithConfirm(selected);
				} else {
					setMessageError("Select document");
				}

			}

			@Override
			public void mouseDown(MouseEvent e) {

			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {

			}
		});

		
		this.btnCancel.addMouseListener(new MouseListener() {
			private static final long serialVersionUID = 201501012306L;

			@Override
			public void mouseUp(MouseEvent e) {
				textSelectDocument.setText("");
				close();
			}

			@Override
			public void mouseDown(MouseEvent e) {

			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {

			}
		});
	}

	
	
	protected void setMessageError(String string) {
		EventBusUISession.post(new EventMessage(Level.ERROR, string));

	}

	/*
	 * Ask confirmation if need. Selection node must be not null.
	 */
	private void deleteWithConfirm(final DocumentNode selected) {
		if (this.btnAskConfirmation.getSelection()) {
			if (checkChildren(selected)) {
				MessageBox confirmation = new MessageBox(
						DialogDeleteDocument.this, SWT.NO | SWT.YES);
				confirmation.setText("Deleting confirmation");
				confirmation
						.setMessage("Document can be not empty. Are you shure?");
				DialogUtil.open(confirmation, new DialogCallback() {
					private static final long serialVersionUID = 201501032310L;

					@Override
					public void dialogClosed(int returnCode) {
						if (returnCode == SWT.YES) {
							delete(selected);
						}
					}
				});
			}
		} else {
			delete(selected);
		}

	}

	private void delete(DocumentNode node) {
		ElementProvider.getInstance().delete(node);
	}

	private boolean checkChildren(DocumentNode node) {
		return ElementProvider.getInstance().hasChildren(node);
	}

	private void databinding() {
		//track selection
		ListenerDocumentNodeSelectionEvent
				.addDocumentSelectionLlistener(new DocumentNodeSelectionListener() {

					@Override
					public void onSelect(DocumentNode selection) {
						setLastSelection(selection);

					}
				});
	}

	@Override
	public void open() {
		super.open();
		//on open - track last selected
		setLastSelection(ListenerDocumentNodeSelectionEvent.getLastSelection());
	}

	private void setLastSelection(DocumentNode selection) {
		String id = selection.getPath();
		id = id == null ? "" : id;
		DialogDeleteDocument.this.textSelectDocument.setText(id);
	}

}
