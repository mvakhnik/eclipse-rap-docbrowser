/**
 * 
 */
package vmk.docbrowser.rap.parts;

import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

/**
 * Implement this interface for possibility open this editor in Docviewer editors.
 * @author Maksim Vakhnik
 *
 */
public interface Editor {

	/**
	 * Gets edit object.
	 * @return
	 */
	Object getEditableData();
	/**
	 * Name of editor.
	 * @return String
	 */
	String getName();
	/**
	 * Main work content of editor. 
	 * @return not null.
	 */
	Control build(Composite parent);
	
	/**
	 * Saves edited content.
	 */
	void save();
	
	/**
	 * 
	 * @return true if exist not saved changes.
	 */
	boolean isDirty();
	
	void addModifyListener(ModifyListener listener);

	void removeModifyListener(ModifyListener listener);
	
	void dispose();
}
