package vmk.docbrowser.rap.parts;


import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ToolTip;
import org.eclipse.swt.widgets.Widget;

/**
 * Composite message's part, style of the part 
 * can be SWT.LEFT(CENTER) | SWT.TOP(BOTTOM, CENTER)
 * @author Maksim Vakhnik
 *
 */
public class CompositeMessage {
	
	private Composite container;
	private ToolTip messageError;
	private ToolTip messageInfo;

	private ToolTip messageWarning;
	
	private int style;
	
	/**
	 * Creates the composite dialog message;
	 * java
	 * @param parent
	 * @param style SWT.LEFT(CENTER) | SWT.TOP(BOTTOM, CENTER)
	 */
	public CompositeMessage(Composite parent, int style) {
		
		this.container = parent;
		this.style = style;
		messageError = new ToolTip(parent.getShell(), SWT.ICON_ERROR );
		messageInfo = new ToolTip(parent.getShell(), SWT.ICON_INFORMATION);
		messageWarning = new ToolTip(parent.getShell(), SWT.ICON_WARNING);
	}

	private void setPosition(ToolTip messager) {
		int xParent = container.getBounds().x;
		int yParent = container.getBounds().y;
		int xCenter = getShiftX();
		int yCenter = getShiftY();
		int x = container.getBounds().x + xParent + xCenter;
		int y = container.getBounds().y + yParent + yCenter;
		Point center = new Point(x, y);
		
		messager.setLocation(center);
	}
	
	private int getShiftX() {
		int values = SWT.LEFT | SWT.RIGHT ;
		switch (this.style & values ) {
		case SWT.RIGHT:
			return container.getBounds().width;
		case SWT.LEFT:
			return 0;
		default:
			return  container.getBounds().width / 2;
		}
	}

	private int getShiftY() {
		int values = SWT.BOTTOM  | SWT.TOP;
		switch (this.style & values ) {
		case SWT.TOP:
			return 0;
		case SWT.BOTTOM:
			return container.getBounds().height;
			
		default:
			return  container.getBounds().height / 2;
		}
	}

	public void showMessageError(String message) {
		hideAll();
		messageError.setText(message);
		setPosition(messageError);
		messageError.setVisible(true);
	}
	
	public void showMessageInfo(String message) {
		hideAll();
		messageInfo.setText(message);
		
		messageInfo.setVisible(true);
		setPosition(messageInfo);
	}
	
	public void showMessageWarning(String message) {
		hideAll();
		messageWarning.setText(message);
		setPosition(messageWarning);
		messageWarning.setVisible(true);
	}
	
	public void hideAll() {
		messageInfo.setVisible(false);
		messageError.setVisible(false);
		messageWarning.setVisible(false);
	}
	/**
	 * Adds listener to widget for hide all messages after closing widget.
	 * @param parent
	 */
	public void addCloseListener(Widget parent) {
		parent.addListener(SWT.Close, new CloseListener());
	}
	
	private class CloseListener implements Listener {
		private static final long serialVersionUID = 201501011607L;
		@Override
		public void handleEvent(Event event) {
			hideAll();
		}
		
	}
}
