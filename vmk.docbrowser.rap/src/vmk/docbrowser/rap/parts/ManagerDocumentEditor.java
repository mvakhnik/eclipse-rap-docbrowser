/**
 * 
 */
package vmk.docbrowser.rap.parts;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import vmk.docbrowser.common.entity.Document;
import vmk.docbrowser.common.entity.DocumentNode;
import vmk.docbrowser.rap.Context;
import vmk.docbrowser.rap.EventBusUISession;
import vmk.docbrowser.rap.events.EventChangeActiveEditor;
import vmk.docbrowser.rap.events.EventOpenDocumentNode;

import com.google.common.base.Objects;
import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;

/**
 * Document manager of the editors. Opens and manages the editors.
 * @author Maksim Vakhnik
 *
 */
public class ManagerDocumentEditor extends Composite{
	
	private static final long serialVersionUID = 1L;
	
	private CTabFolder tabFolder;
	private Map<CTabItem, Listener> listeners;
	public ManagerDocumentEditor(Composite parent) {
		super(parent, SWT.NONE);
		this.setLayout(new FillLayout());
		listeners = new HashMap<>();
		initContext();
		createUI();
		addListener();
	}

	private void initContext() {
		if (! Context.setComponent(ManagerDocumentEditor.class.getName(), this)) {
			throw new IllegalStateException("Can`t register " + ManagerDocumentEditor.class.getName() 
					+ " instance. Maybe it is registered already.");
		}
	}

	private void addListener() {
		EventBusUISession.register(this);
		

	}

	private void createUI() {
//		view = new CompositeDocumentEditor(this, SWT.NONE);
		tabFolder = new CTabFolder(this, SWT.BORDER);
	}

	@Subscribe
	@AllowConcurrentEvents
	public void onOpenDocument(EventOpenDocumentNode event) {
		openEditor(event.getObject());

	}

	public void openEditor(DocumentNode document) {
		if (document != null && ! document.isDirectory()) {
			if (isOpened(document) == null) {
				Editor editor = createEditor((Document) document);
				openEditor(editor);
				EventBusUISession.post(new EventChangeActiveEditor(editor));
			}
		}
	}
	
	private Editor createEditor(Document document) {
		
		Editor editor = new DocumentEditor(document);
		return editor;
	}

	/**
	 * Open editors tab.
	 * @param editor - not null.
	 */
	private void openEditor(final Editor editor) {
		final CTabItem item = new CTabItem(tabFolder, SWT.CLOSE);
		item.setControl(editor.build(tabFolder));
		item.setText(editor.getName());
		//set editor as items data 
		item.setData(editor);
		
		
		editor.addModifyListener(new ModifyListener() {
			private static final long serialVersionUID = 201501061530L;
			private Editor currentEditor = editor;
			private CTabItem tabItem = item;
			@Override
			public void modifyText(ModifyEvent event) {
				if(currentEditor.isDirty()) {
					tabItem.setText("*" + currentEditor.getName());
					EventBusUISession.post(new EventChangeActiveEditor((Editor) item.getData()));
				} else {
					tabItem.setText(currentEditor.getName());
					EventBusUISession.post(new EventChangeActiveEditor((Editor) item.getData()));
				}
			}
		});

		//remove FocusListeners
		item.addDisposeListener(new DisposeListener() {
			private static final long serialVersionUID = 201501142156L;
			@Override
			public void widgetDisposed(DisposeEvent event) {
				EventBusUISession.post(new EventChangeActiveEditor(null));
				tabFolder.removeListener(SWT.FocusIn, listeners.get(item));
				listeners.remove(item);
				
			}
		});
		
		FocusListener listener = new FocusListener(item);
		listeners.put(item, listener);
		tabFolder.addListener(SWT.FocusIn, listener);
		tabFolder.setSelection(tabFolder.getItemCount() - 1);

	}

	private class FocusListener implements Listener {
		private static final long serialVersionUID = 201501142156L;
		private final CTabItem item;
		
		FocusListener(CTabItem item) {
			this.item = item;
		}
		
		@Override
		public void handleEvent(Event event) {
			if (! item.isDisposed() && item.getControl().isVisible()) {
				EventBusUISession.post(new EventChangeActiveEditor((Editor) item.getData()));
				item.getControl().setFocus();
			} 
			
		}
		
	}
	
	private CTabItem isOpened(DocumentNode document) {
		for (CTabItem item : getOpenedItems()) {
			Editor editor = (Editor) item.getData();
			if (Objects.equal(editor.getEditableData(), document)) {
				return item;
			}
		}
		return null;
	}

	private Collection<CTabItem> getOpenedItems() {
		return Arrays.asList(this.tabFolder.getItems());
	}
	
	public static ManagerDocumentEditor getInstance() {
		return (ManagerDocumentEditor) Context.getComponent(ManagerDocumentEditor.class.getName());
	}

	public Editor getActiveEditor() {
		if (tabFolder.getSelectionIndex() > -1) {
			return (Editor) tabFolder.getItem(tabFolder.getSelectionIndex()).getData();
		}
		return null;
	}
}
