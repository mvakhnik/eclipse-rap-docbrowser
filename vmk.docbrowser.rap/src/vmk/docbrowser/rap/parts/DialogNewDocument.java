/**
 * 
 */
package vmk.docbrowser.rap.parts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.widgets.Shell;

import vmk.docbrowser.common.entity.DocumentDirectory;
import vmk.docbrowser.common.entity.DocumentNode;
import vmk.docbrowser.rap.UIContextDialog;
import vmk.docbrowser.rap.actions.ActionCreateNewDocument;
import vmk.docbrowser.rap.listener.ListenerDocumentNodeSelectionEvent;
import vmk.docbrowser.rap.listener.ListenerDocumentNodeSelectionEvent.DocumentNodeSelectionListener;

/**
 * UI part: dialog of document's creation.
 * @author Maksim Vakhnik
 *
 */
public class DialogNewDocument extends UIContextDialog {

	private static final long serialVersionUID = 201501042206L;

	private CompositeDialog content;
	private DocumentDirectory selectedDirectory;

	public DialogNewDocument(Shell shell, int style) {
		super(DialogNewDocument.class, shell, style);
		setContent();
		addListeners();
	}

	private void addListeners() {

		ListenerDocumentNodeSelectionEvent
				.addDocumentSelectionLlistener(new DocumentNodeSelectionListener() {

					@Override
					public void onSelect(DocumentNode selected) {
						if (selected != null && selected.isDirectory()) {
							setSelectedDirectory((DocumentDirectory) selected);
							String id = selected.getPath();
							id = id == null ? "" : id;
							content.getText0().setText(id);

						}

					}
				});
		content.getBtnOk().addMouseListener(new MouseListener() {

			private static final long serialVersionUID = 201501051044L;

			@Override
			public void mouseUp(MouseEvent e) {
				ActionCreateNewDocument.create(getSelectedDirectory(), content.getText1()
						.getText());

			}

			@Override
			public void mouseDown(MouseEvent e) {

			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {

			}
		});

		content.getBtnCancel().addMouseListener(new MouseListener() {
			private static final long serialVersionUID = 201501051044L;

			@Override
			public void mouseUp(MouseEvent e) {
				close();
			}

			@Override
			public void mouseDown(MouseEvent e) {
				// do nothing

			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				// do nothing

			}
		});
	}

	public void setContent() {
		content = new CompositeDialog(this, SWT.NONE);
		this.setText("Create new document");
		content.getLabel0().setText("Select parent directory");
		content.getLabel1().setText("Input new document name");
		content.getBtnOk().setText("Create");
		content.getBtnCancel().setText("Cancel");

	}

	private DocumentDirectory getSelectedDirectory() {
		return selectedDirectory;
	}

	private void setSelectedDirectory(DocumentDirectory selectedDirectory) {
		this.selectedDirectory = selectedDirectory;
	}

}
