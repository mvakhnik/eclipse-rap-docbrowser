/**
 * Basic entry point. Resolve all needed dependencies, set context, start gui.
 */

package vmk.docbrowser.rap;

import java.util.Date;
import java.util.Enumeration;

import org.eclipse.rap.rwt.RWT;
import org.eclipse.rap.rwt.application.AbstractEntryPoint;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

/**
 * Application's entry point. 
 * @author Maksim Vakhnik
 *
 */
public class DocbrowserEntryPoint extends AbstractEntryPoint {

	private Composite composite;
	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	protected void createContents(Composite parent) {
		final Composite shell = parent;
		
		shell.setLayout(new FillLayout());
		shell.setBackground(parent.getDisplay().getSystemColor(
				SWT.COLOR_DARK_GRAY));
		setComposite(shell);
		new Resources();
		new Context(this);
		logRequestInfo();

	
//		Logger.getInstance().log(message);
	}

	/*
	 * Logs all application's creations.
	 */
	private void logRequestInfo() {
		@SuppressWarnings("rawtypes")
		Enumeration message = RWT.getRequest().getHeaderNames();
		StringBuilder header = new StringBuilder("Request info:");
		header.append(String.valueOf(new Date(System.currentTimeMillis())));
		header.append(":");
		while (message.hasMoreElements()) {
			Object nextElement = message.nextElement();
			
			header.append("[");
			header.append(String.valueOf(nextElement));
			header.append(":");
			header.append(RWT.getRequest().getHeader((String) nextElement));
			header.append("]");
		}
		Logger.getInstance().log(header.toString());
	}

	public Composite getComposite() {
		return composite;
	}


	private void setComposite(Composite composite) {
		this.composite = composite;
	}
	
	

}
