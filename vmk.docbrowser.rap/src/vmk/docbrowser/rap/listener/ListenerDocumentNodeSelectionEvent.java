/**
 * 
 */
package vmk.docbrowser.rap.listener;

import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.rap.rwt.RWT;

import vmk.docbrowser.common.entity.DocumentNode;
import vmk.docbrowser.rap.EventBusUISession;
import vmk.docbrowser.rap.events.EventSelectDocumentNode;
import vmk.docbrowser.rap.exceptions.NodeNotExistException;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;

/**
 * Uses for listen tree node selection on UIContext. Contains history of
 * previous selection.
 * 
 * @author Maksim Vakhnik
 *
 */
public class ListenerDocumentNodeSelectionEvent {
	public interface DocumentNodeSelectionListener {
		void onSelect(DocumentNode selection);
	}

	private Set<DocumentNodeSelectionListener> listeners;

	private ArrayDeque<DocumentNode> stack;
	private final byte dequeSize = 2;

	private ListenerDocumentNodeSelectionEvent() {
		initInstance();
		EventBusUISession.register(this);

		listeners = new HashSet<>();
		stack = new ArrayDeque<>(2);
	}

	public static void addDocumentSelectionLlistener(
			DocumentNodeSelectionListener listener) {
		getInstance().listeners.add(listener);
	}

	public static void removeTreeNodeSelectionLlistener(
			DocumentNodeSelectionListener listener) {
		getInstance().listeners.remove(listener);
	}

	/**
	 * Get component instance from UISession context.
	 * 
	 * @return DialogNewDirectory instance.
	 */
	private static ListenerDocumentNodeSelectionEvent getInstance() {
		Object instance = RWT.getUISession().getAttribute(
				ListenerDocumentNodeSelectionEvent.class.getName());
		if (instance == null) {
			instance = new ListenerDocumentNodeSelectionEvent();
		}
		return (ListenerDocumentNodeSelectionEvent) instance;
	}

	private void initInstance() {
		RWT.getUISession().setAttribute(
				ListenerDocumentNodeSelectionEvent.class.getName(), this);
	}

	/**
	 * Process EventSelectDocument, invoked by {@code EventBusUISession}.
	 * 
	 * @param event
	 */
	@Subscribe
	@AllowConcurrentEvents
	public void selectDocumentEvent(EventSelectDocumentNode event) {
		try {
			DocumentNode selection = event.getNode();
			push(selection);
			onSelectNode(selection);
		} catch (NodeNotExistException e) {
			//do nothing
		}
	}


	
	private void push(DocumentNode node) {
		if (stack.size() == dequeSize) {
			stack.removeLast();
		}
		stack.push(node);
	}

	/**
	 * Get last selected DocumentNode.
	 * @return
	 */
	public static DocumentNode getLastSelection() {
		return getInstance().stack.peekFirst();
	}

	/**
	 * Get previous selected DocumentNode.
	 * @return
	 */
	public static DocumentNode getPreviousSelection() {
		return getInstance().stack.peekLast();
	}

	private void onSelectNode(DocumentNode treeNode) {
		for (DocumentNodeSelectionListener listener : listeners) {
			listener.onSelect(treeNode);
		}
	}
}
