/**
 * 
 */
package vmk.docbrowser.rap;

import java.util.Objects;

import org.eclipse.rap.rwt.RWT;

import vmk.docbrowser.common.entity.DocumentNode;
import vmk.docbrowser.rap.events.EventDocumentDeleted;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;

/**
 * Stores copied, clipped {@code DocumentNode} for further pasting. Exists in RWT.UISession context.
 * Accessible directly through static methods.
 * @author Maksim Vakhnik
 *
 */
public class DocumentBuffer {
	public enum Action {
		CUT,
		COPY
	}
	private DocumentNode pasted;
	private Action action;
	
	private DocumentBuffer() {
		initInstance();
		
	}

	public static DocumentBuffer getInstance() {
		Object instance =  RWT.getUISession().getAttribute(DocumentBuffer.class.getName());
		if (instance == null) {
			 instance = new DocumentBuffer();
		}
		return  (DocumentBuffer) instance;
	}
	
	private  void initInstance() {
		RWT.getUISession().setAttribute(DocumentBuffer.class.getName(), this);
		EventBusUISession.register(this);
	}
	
	/**
	 * Clear buffer if document deleted.
	 * @param event
	 */
	@Subscribe
	@AllowConcurrentEvents
	public void selectDocumentEvent(EventDocumentDeleted event) {
		if (this.pasted != null) {
			Object object = event.getObject();
			if (Objects.equals(object, this.pasted)) {
				clear();
			}
		}
	}
	
	public  DocumentNode getPasted() {
		return pasted;
	}

	public  void cut(DocumentNode pasted) {
		this.pasted = pasted;
		action = Action.CUT;
		
	}
	
	public  void copy(DocumentNode pasted) {
		this.pasted = pasted;
		action = Action.COPY;
		
	}

	/**
	 * Get last {@code Action}
	 * @return {@code Buffer.Action}
	 */
	public Action getAction() {
		return action;
	}
	
	/**
	 * Clear buffer.
	 */
	public void clear() {
		pasted = null;
		action = null;
	}
	
	
}
