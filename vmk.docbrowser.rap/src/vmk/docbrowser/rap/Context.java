/**
 * Contains context of RAP application. 
 * Start application event bus, document provider
 */
package vmk.docbrowser.rap;

import org.eclipse.rap.rwt.RWT;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;

import vmk.docbrowser.common.provider.Documents;
import vmk.docbrowser.rap.docviewer.DocumentViewer;
import vmk.docbrowser.rap.parts.ContentPart;
import vmk.docbrowser.rap.parts.DialogDeleteDocument;
import vmk.docbrowser.rap.parts.DialogNewDirectory;
import vmk.docbrowser.rap.parts.DialogNewDocument;
import vmk.docbrowser.rap.parts.DialogRename;
import vmk.docbrowser.rap.parts.MainWindow;
import vmk.docbrowser.rap.parts.ManagerDocumentEditor;
import vmk.docbrowser.rap.parts.MenuBar;

/**
 * The application's controller. Builds the UI, receives {@code Documents} from the bundle's context.
 * @author Maksim Vakhnik
 *
 */
public class Context {
	
	private DocumentViewer documentViewer;
	
	/**
	 * Creates the application's context.
	 * @param entryPoint - {@code EntryPoint}, not null
	 */
	public Context(DocbrowserEntryPoint entryPoint) {
		initApplicationContext();
		createUI(entryPoint.getComposite());
	}

	private void initApplicationContext() {

		Documents documents = getImplementation(Documents.class);
		setComponent(Documents.class.getName(), documents);
		
	}

	/**
	 * Gets {@code Documents} from the OSGi bundle context.
	 * @return
	 */
	public static Documents getDocuments() {
			Documents documents = getImplementation(Documents.class);
			return documents;
	}
	/**
	 * Gets a component from the HTTPSession context.
	 * @param nameObject - usually class.name of needed component
	 * @return needed component, null if not exist.
	 */
	public static Object getComponent(String nameObject) {
		Object result = RWT.getUISession().getHttpSession().getAttribute(createComponentName(nameObject));
		return result;
	}
	
	private static String createComponentName(String name) {
		return String.valueOf(name.hashCode());
	}
	
	/**
	 * Registers a component in the application's HTTPcontext.
	 * @param name of component (class.name)
	 * @param value - object for registration.
	 * @return true if success, false if element with this name exist
	 */
	public static boolean setComponent(String name, Object value) {
		String tmpName = createComponentName(name);
		if (getComponent(tmpName) == null) {
			RWT.getUISession().getHttpSession().setAttribute(tmpName, value);
			return true;
		}
		return false;
		
	}
	 
	/*
	 * Gets a service from bundle context.
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static  <T> T getImplementation(Class<T> clazz) {

		BundleContext bc = FrameworkUtil.getBundle(clazz).getBundleContext();

		ServiceReference service = bc.getServiceReference(clazz.getName());
		if (service != null) {
			return (T) bc.getService(service);
		}
		return null;
	}
	
	private void createUI(Composite parent) {
		MainWindow mainWindow = new MainWindow(parent, SWT.NONE);
		ContentPart content = new ContentPart(mainWindow.getViewForm(),
				SWT.NONE);
		
		this.documentViewer = new DocumentViewer(content.getLeftPart(), SWT.NONE);
		
		mainWindow.getViewForm().setContent(content);
		//
		MenuBar menuBar = new MenuBar(mainWindow.getViewForm(), SWT.NONE);
		mainWindow.getViewForm().setTopLeft(menuBar);
//
		new ManagerDocumentEditor(content.getRightPart());
		new DialogNewDirectory(parent.getShell(), SWT.DIALOG_TRIM);
		new DialogNewDocument(parent.getShell(), SWT.DIALOG_TRIM);
		new DialogDeleteDocument(parent.getShell(), SWT.DIALOG_TRIM);
		new DialogRename(parent.getShell(), SWT.DIALOG_TRIM);
		
		new UIMessager(mainWindow.getViewForm(), SWT.CENTER | SWT.TOP );
	}


	/**
	 * Gets the {@code DocumentViewer} instance.
	 * @return DocumentViewer
	 */
	public DocumentViewer getDocumentViewer() {
		return documentViewer;
	}

}
