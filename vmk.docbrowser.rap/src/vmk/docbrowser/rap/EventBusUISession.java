/**
 * 
 */
package vmk.docbrowser.rap;

import org.eclipse.rap.rwt.RWT;

import com.google.common.eventbus.EventBus;

/**
 * Application UIContext event bus.
 * For receive posts need two annotations {@code @Subscribe, @AllowConcurrentEvents}.
 * Its annotated public void method with Event some type.
 * <i>
 *  <p />
 * Example:
 * <p />
 *{@code @Subscribe @AllowConcurrentEvents}  <p />
 *  	public void selectDocumentEvent(EventSelectDocumentNode event) { <p />do something <p />} 
 * </i>
 * 
 * 
 * @author Maksim Vakhnik
 *
 */
public class EventBusUISession {
	private final EventBus eventBus;
	
	public static void register(Object object) {
		getInstance().eventBus.register(object);
	}
	
	public static void post(Object object) {
		getInstance().eventBus.post(object);
	}
	
	
	private EventBusUISession() {
		this.eventBus = new EventBus();
		Logger.getInstance().log("init EventBusUISession:" + eventBus);
		initInstance();
	}

	private static EventBusUISession getInstance() {
		Object instance = RWT.getUISession().getAttribute(
				EventBusUISession.class.getName());
		if (instance == null) {
			instance = new EventBusUISession();
		}
		return (EventBusUISession) instance;
	}

	private void initInstance() {
		RWT.getUISession()
				.setAttribute(EventBusUISession.class.getName(), this);
	}
	
}
