/**
 * 
 */
package vmk.docbrowser.rap.actions;

import vmk.docbrowser.rap.UIContextDialog;
import vmk.docbrowser.rap.parts.DialogNewDocument;

/**
 * Starts the dialog of document's creation.
 * @author Maksim Vakhnik
 *
 */
public class ActionStartNewDocumentDialog {

	public static void  run() {
		DialogNewDocument dialog = UIContextDialog.getInstance(DialogNewDocument.class);
		dialog.open();
	}
}
