/**
 * 
 */
package vmk.docbrowser.rap.actions;

import vmk.docbrowser.common.entity.DocumentNode;
import vmk.docbrowser.rap.parts.ManagerDocumentEditor;

/**
 * Opens a needed document's editor.
 * @author Maksim Vakhnik
 *
 */
public class ActionOpenDocumentEditor {

	public static void edit(DocumentNode document) {
		if (document != null && ! document.isDirectory()) {
			ManagerDocumentEditor manager = ManagerDocumentEditor.getInstance();
			manager.openEditor(document);
		}
	}
}
