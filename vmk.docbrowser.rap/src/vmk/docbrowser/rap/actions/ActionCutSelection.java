/**
 * 
 */
package vmk.docbrowser.rap.actions;

import vmk.docbrowser.common.entity.DocumentNode;
import vmk.docbrowser.rap.DocumentBuffer;
import vmk.docbrowser.rap.listener.ListenerDocumentNodeSelectionEvent;


/**
 * 
 * Cuts the last selected document node in to the buffer.
 * @author Maksim Vakhnik
 *
 */
public class ActionCutSelection {

	public static void run() {
		DocumentNode selected = ListenerDocumentNodeSelectionEvent.getLastSelection();
		if (selected != null) {
			//cut to buffer
			DocumentBuffer.getInstance().cut(selected);
		}
		
	}
}
