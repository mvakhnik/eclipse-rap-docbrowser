/**
 * 
 */
package vmk.docbrowser.rap.actions;

import vmk.docbrowser.common.entity.DocumentNode;
import vmk.docbrowser.rap.DocumentBuffer;
import vmk.docbrowser.rap.listener.ListenerDocumentNodeSelectionEvent;


/**
 * Copies selected TreeNode to the {@code Buffer}.
 * @author Maksim Vakhnik
 *
 */
public class ActionCopySelection {

	public static void run() {
		DocumentNode selected = ListenerDocumentNodeSelectionEvent.getLastSelection();
		if (selected != null) {
			//cut to buffer
			DocumentBuffer.getInstance().copy(selected);
		}
	}

}
