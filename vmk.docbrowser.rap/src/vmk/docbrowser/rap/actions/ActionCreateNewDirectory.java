/**
 * 
 */
package vmk.docbrowser.rap.actions;

import vmk.docbrowser.common.entity.DocumentDirectory;
import vmk.docbrowser.rap.docviewer.ElementProvider;


/**
 * Creates the new directory in  parent's directory.
 * @author Maksim Vakhnik
 *
 */
public  class ActionCreateNewDirectory  {

	public static void create(DocumentDirectory parent, String name) {
		ElementProvider.getInstance().createDirectory(parent, name);
	}
}
