/**
 * 
 */
package vmk.docbrowser.rap.actions;

import vmk.docbrowser.common.entity.DocumentNode;
import vmk.docbrowser.rap.docviewer.ElementProvider;

/**
 * Renames the document.
 * @author Maksim Vakhnik
 *
 */
public class ActionRenameDocument {

	/**
	 * Renames the document.
	 * @param document
	 * @param name, not null
	 */
	public static void rename (DocumentNode document, String name) {
		ElementProvider.getInstance().rename(document, name);
	}
}
