/**
 * 
 */
package vmk.docbrowser.rap.actions;

import vmk.docbrowser.rap.parts.DialogRename;

/**
 * Starts the renaming dialog.
 * @author Maksim Vakhnik
 *
 */
public class ActionStartDocumentRenaming {

	public static void run() {
		DialogRename.getInstance(DialogRename.class).open();
	}
}
