/**
 * 
 */
package vmk.docbrowser.rap.actions;

import vmk.docbrowser.rap.parts.DialogNewDirectory;

/**
 * Starts the directory's creation dialog.
 * @author Maksim Vakhnik
 *
 */
public class ActionStartNewDirectoryDialog {
	
	public static void run() {
		DialogNewDirectory dialog = DialogNewDirectory.getInstance(DialogNewDirectory.class);
		if (dialog != null) {
			dialog.open();
		}
		
	}
}
