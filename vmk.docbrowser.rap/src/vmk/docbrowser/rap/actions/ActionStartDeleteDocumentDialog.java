/**
 * 
 */
package vmk.docbrowser.rap.actions;

import vmk.docbrowser.rap.parts.DialogDeleteDocument;

/**
 * Starts the dialog of document's deleting.
 * @author Maksim Vakhnik
 *
 */
public class ActionStartDeleteDocumentDialog {

	public static void  run() {
		DialogDeleteDocument.getInstance(DialogDeleteDocument.class).open();
	}
}
