/**
 * 
 */
package vmk.docbrowser.rap.actions;

import vmk.docbrowser.common.entity.DocumentDirectory;
import vmk.docbrowser.rap.docviewer.ElementProvider;


/**
 * Creates the new document in parent's directory.
 * 
 * @author Maksim Vakhnik
 *
 */
public abstract class ActionCreateNewDocument {

	/**
	 * Create empty Document in parent directory.
	 * @param parent
	 * @param name
	 */
	public static void create(DocumentDirectory parent, String name) {
		ElementProvider.getInstance().createDocument(parent, name);
	}

}
