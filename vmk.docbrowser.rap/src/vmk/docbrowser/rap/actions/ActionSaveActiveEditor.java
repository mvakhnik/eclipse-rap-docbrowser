/**
 * 
 */
package vmk.docbrowser.rap.actions;

import vmk.docbrowser.rap.parts.Editor;
import vmk.docbrowser.rap.parts.ManagerDocumentEditor;

/**
 * Saves the content of active editor. 
 * @author Maksim Vakhnik
 *
 */
public class ActionSaveActiveEditor {

	public static void run() {
		Editor editor = ManagerDocumentEditor.getInstance().getActiveEditor();
		if (editor != null) {
			editor.save();
		}
	}
}
