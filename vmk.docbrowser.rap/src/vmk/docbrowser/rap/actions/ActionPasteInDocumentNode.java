package vmk.docbrowser.rap.actions;

import vmk.docbrowser.common.entity.DocumentDirectory;
import vmk.docbrowser.common.entity.DocumentNode;
import vmk.docbrowser.rap.DocumentBuffer;
import vmk.docbrowser.rap.DocumentBuffer.Action;
import vmk.docbrowser.rap.docviewer.ElementProvider;
import vmk.docbrowser.rap.listener.ListenerDocumentNodeSelectionEvent;


/**
 * Pastes from the {@code Buffer} to selected DocumentNode if both supports.
 * Remove pasted if needed from source.
 */
public class ActionPasteInDocumentNode {

		public static void run() {
			DocumentBuffer buffer = DocumentBuffer.getInstance();
			DocumentNode src = buffer.getPasted();
			final Action action = buffer.getAction();
			if (src != null) {
				DocumentNode source = (DocumentNode) src;
				DocumentNode docTarget = ListenerDocumentNodeSelectionEvent.getLastSelection();
				if (docTarget.isDirectory()) {
					DocumentDirectory target = (DocumentDirectory) docTarget;
					if (action == Action.COPY) {
						ElementProvider.getInstance().copy(source, target);
					}
					if (action == Action.CUT) {
						ElementProvider.getInstance().move(source, target);
						buffer.clear();
					}
					
				}
					
			}
			
		}

}
