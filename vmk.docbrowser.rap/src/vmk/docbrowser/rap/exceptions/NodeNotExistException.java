/**
 * 
 */
package vmk.docbrowser.rap.exceptions;

import java.io.IOException;

/**
 * TreeNode is not exist already.
 * @author Maksim Vakhnik
 *
 */
public class NodeNotExistException extends IOException {

	private static final long serialVersionUID = 201501041323L;

	public NodeNotExistException() {
		super();
	}

	public NodeNotExistException(String message, Throwable cause) {
		super(message, cause);
	}

	public NodeNotExistException(String message) {
		super(message);
	}

	public NodeNotExistException(Throwable cause) {
		super(cause);
	}

	
}
