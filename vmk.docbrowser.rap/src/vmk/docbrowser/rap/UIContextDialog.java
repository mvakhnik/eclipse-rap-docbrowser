/**
 * 
 */
package vmk.docbrowser.rap;

import org.eclipse.rap.rwt.RWT;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

/**
 * Creates dialog with auto centering and auto resizing. Resizing can be set by overriding 
  *{@code UIContextDialog.heigh, UIContextDialog.width}. <p />
  *Add to shell nodisposing listener (shell will not be dispose after closing - just hide). Override 
  *{@code Shell.open()} for immediately focus on component. <p />
 * Register implemented subclass in UIContext, it can be received from context
 * through {@code UIContextDialog.getInstance(Class<T> component)}.<p />
 * @author Maksim Vakhnik
 *
 */
public abstract class UIContextDialog extends Shell {
	// window max size
	protected int height = 300;
	protected int width = 450;

	private static final long serialVersionUID = 201501031956L;

	// private T instance;

	protected <T> UIContextDialog(Class<T> component, Shell shell, int style) {
		super(shell, style);
		setLayout(new FormLayout());
		initInstance(component);
		initSizePosition(this);
		addNoCloseListener();
	}

	private void addNoCloseListener() {
		// shell will not be disposed, just hidden
		this.addListener(SWT.Close, new Listener() {
			private static final long serialVersionUID = 201501011607L;

			@Override
			public void handleEvent(Event event) {
				event.doit = false;
				setVisible(false);
			}
		});

	}

	private <T> void initInstance(Class<T> component) {
		RWT.getUISession().setAttribute(component.getName(), this);
	}

	/**
	 * Gets component's instance from the UISession's context.
	 * 
	 * @return DialogNewDirectory instance.
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getInstance(Class<T> component) {
		return (T) RWT.getUISession().getAttribute(component.getName());
	}

	/*
	 * Set size of window dynamically.
	 */
	private void initSizePosition(Shell shell) {

		int h = shell.getDisplay().getClientArea().height;
		int w = shell.getDisplay().getClientArea().width;
		int maxW = w * 9 / 10;
		int maxH = h * 9 / 10;
		shell.setSize(width < maxW ? width : maxW, height < maxH ? height
				: maxH);
		Rectangle bounds = getDisplay().getBounds();
		Rectangle rect = shell.getBounds();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		shell.setLocation(x, y);
	}

	/**
	 * Open and  focus on window.
	 */
	@Override
	public void open() {
		this.getShell().setFocus();
		super.open();

	}

}
