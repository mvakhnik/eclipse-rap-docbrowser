/**
 * 
 */
package vmk.docbrowser.rap.events;

import vmk.docbrowser.common.entity.DocumentNode;
import vmk.docbrowser.rap.exceptions.NodeNotExistException;


/**
 * Event sent/received when node selection is changed.
 * @author Maksim Vakhnik
 *
 */
public class EventSelectDocumentNode  {
	
	private final DocumentNode document;

	public EventSelectDocumentNode(DocumentNode node) {
		this.document = node;
	}
	
	public DocumentNode getNode() throws NodeNotExistException {
		if (document == null) {
			throw new NodeNotExistException("Node not exist");
		}
		return document;
	}
	
	public DocumentNode getDocument() {
		
		return document;
	}
}
