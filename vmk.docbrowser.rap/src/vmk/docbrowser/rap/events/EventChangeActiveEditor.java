/**
 * 
 */
package vmk.docbrowser.rap.events;

import vmk.docbrowser.rap.parts.Editor;

/**
 * Posted when document content is changed. 
 * @author Maksim Vakhnik
 *
 */
public class EventChangeActiveEditor {

	private final Editor object;
	
	/**
	 *  Posts when document content changed.
	 * @param editor - ActivatedEditor, can be null.
	 */
	public EventChangeActiveEditor(Editor editor) {
		this.object = editor;
	}

	public Editor getObject() {
		return object;
	}

		
}
