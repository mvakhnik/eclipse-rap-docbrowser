/**
 * 
 */
package vmk.docbrowser.rap.events;

import vmk.docbrowser.common.entity.DocumentNode;

/**
 * Posts when DocumentNode must be opened.
 * @author Maksim Vakhnik
 *
 */
public class EventOpenDocumentNode {

	private final DocumentNode object;
	
	public EventOpenDocumentNode(DocumentNode object) {
		this.object = object;
	}

	public DocumentNode getObject() {
		return this.object;
	}

}
