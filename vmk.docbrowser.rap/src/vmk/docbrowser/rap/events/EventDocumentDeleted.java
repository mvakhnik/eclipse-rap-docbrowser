/**
 * 
 */
package vmk.docbrowser.rap.events;

import vmk.docbrowser.common.entity.DocumentNode;

/**
 *  Event invoked when document is deleted.
 * @author Maksim Vakhnik
 *
 */
public class EventDocumentDeleted {

	private final DocumentNode deleted;

	public EventDocumentDeleted(DocumentNode deleted) {
		this.deleted = deleted;
	}

	public DocumentNode getObject() {
		return deleted;
	}
	
	
}
