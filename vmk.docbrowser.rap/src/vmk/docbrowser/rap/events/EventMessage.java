/**
 * 
 */
package vmk.docbrowser.rap.events;

/**
 * Used for the message system of the application.
 * @author Maksim Vakhnik
 *
 */
public class EventMessage {
	
	public enum Level {
		INFO,
		WARNING,
		ERROR
	}
	private final Level level;
	private final String message;
	
	public EventMessage(Level level, String message) {
		super();
		this.level = level;
		this.message = message;
	}

	public Level getLevel() {
		return level;
	}

	public String getMessage() {
		return message;
	}

	
}
