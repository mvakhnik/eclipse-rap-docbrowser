/**
 * 
 */
package vmk.docbrowser.rap.events;

import vmk.docbrowser.common.entity.DocumentNode;

/**
 * Event invoked when new document is created.
 * @author Maksim Vakhnik
 *
 */
public class EventDocumentCreated {

	private final DocumentNode document;

	public EventDocumentCreated(DocumentNode document) {
		super();
		this.document = document;
	}

	public DocumentNode getObject() {
		return document;
	}
	
	
}
