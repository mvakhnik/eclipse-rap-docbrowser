/**
 * 
 */
package vmk.docbrowser.rap;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.rap.rwt.RWT;
import org.eclipse.rap.rwt.service.ResourceManager;
import org.eclipse.swt.graphics.Image;

/**
 * Static resources manager.
 * 
 * @author Maksim Vakhnik
 *
 */
public class Resources {
	
	/**
	 * Resources. For getting resources use {@code Resources.getImage()}
	 * 
	 * @author Maksim Vakhnik
	 *
	 */
	public enum Images {
		ICON_DIRECTORY("resources/images/icon-directory.png"), ICON_DOCUMENT(
				"resources/images/icon-document.png"), ICON_ROOT(
				"resources/images/icon-root.png");

		private final String url;
		private Image image;
		
		private Images( String url) {
			this.url = url;
		}

		private String getUrl() {
			return url;
		}


		public Image getImage() {
			return image;
		}

		private void setImage(Image image) {
			this.image = image;
		}
	}

	private static URL applicationUrl;

	/**
	 * Initializes resources. 
	 */
	public Resources() {
		initImages();
	}

	private void initImages() {
		try {
			Resources.applicationUrl = new URL(RWT.getRequest()
					.getRequestURL().toString());
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		ResourceManager resourceManager = RWT.getResourceManager();
		for (Images resource : Images.values()) {
			if (!resourceManager.isRegistered(resource.getUrl())) {
				try (InputStream inputStream = this.getClass().getClassLoader()
						.getResourceAsStream(resource.getUrl());) {
					resourceManager.register(resource.getUrl(), inputStream);
					resource.setImage(createImage(resource));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private Image createImage(Resources.Images resource) {
		URL imgUrl = getRealUrl(resource.getUrl());
		ImageDescriptor id = ImageDescriptor.createFromURL(imgUrl);
		Image img = id.createImage();
		return img;
	}
	

	private String getLocation(String localResourcePath) {
		return RWT.getResourceManager().getLocation(localResourcePath);
	}

	/*
	 * Create real url for client (http://<app-url>/<resources-path>/<resource>
	 */
	private URL getRealUrl(String localResourcePath) {
		try {
			return new URL(applicationUrl, getLocation(localResourcePath));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
