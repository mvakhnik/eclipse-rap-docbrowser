/**
 * 
 */
package vmk.docbrowser.rap.messages;

import java.util.ResourceBundle;

import vmk.docbrowser.common.provider.DocumentOperationResult;

/**
 * Gets localized messages from ResultMessages.properties file. If the file is not exist - return default 
 * messages from {@code Result.getMessage()};
 * @author Maksim Vakhnik
 *
 */
public class ResultMessage {
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("vmk.docbrowser.rap.messages.ResultMessages"); 
	static {
	}
	public static String getMessage(DocumentOperationResult result) {
		String message;
		try {
			message = BUNDLE.getString(result.name());
		} catch (Exception e) {
			 message = result.getMessage();
		}
		return message;
	}
}
