/**
 * 
 */
package vmk.docbrowser.rap;

import org.eclipse.swt.widgets.Composite;

import vmk.docbrowser.rap.events.EventMessage;
import vmk.docbrowser.rap.parts.CompositeMessage;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;

/**
 * Intercepts messages, shows received messages.
 * @author Maksim Vakhnik
 *
 */
public class UIMessager {


	private CompositeMessage view;
	
	/**
	 * Messages will be showed in parent area, as default - in left corner.
	 * @param parent - container for messages.
	 * @param positionStyle -  SWT.LEFT(RIGHT, CENTER) | SWT.TOP(BOTTOM, CENTER) | SWT.NONE
	 * message will be showed in defined corner
	 */
	public UIMessager(Composite parent, int positionStyle) {
		view = new CompositeMessage(parent, positionStyle);
		initInstance();
	}

	private void initInstance() {
		EventBusUISession.register(this);
	}

	@Subscribe
	@AllowConcurrentEvents
	public void onMessageReceive(EventMessage event) {
		switch (event.getLevel()) {
		case INFO:
			view.showMessageInfo(event.getMessage());
			break;
		case ERROR:
			view.showMessageError(event.getMessage());
			break;
		case WARNING:
			view.showMessageWarning(event.getMessage());
			break;
		default:
			break;
		}
	}
	


	
}
