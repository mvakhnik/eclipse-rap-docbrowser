/**
 * 
 */
package vmk.docbrowser.rap.docviewer;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

/**
 * Content provider for a tree viewer.
 * @author Maksim Vakhnik
 *
 */
public class ContentProviderDocuments  implements ITreeContentProvider {

	private static final long serialVersionUID = 201412291908L;

	private final ElementProvider elementProvider;
	
	public ContentProviderDocuments(ElementProvider elementProvider) {
		this.elementProvider = elementProvider;
	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		
	}

	@Override
	public Object[] getElements(Object inputElement) {
		return elementProvider.getElements(inputElement);
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		return elementProvider.getChildren(parentElement);
	}

	@Override
	public Object getParent(Object element) {
		return elementProvider.getParent(element);
	}

	@Override
	public boolean hasChildren(Object element) {
		boolean hasChildren = elementProvider.hasChildren(element);
		
		return hasChildren;
	}

}
