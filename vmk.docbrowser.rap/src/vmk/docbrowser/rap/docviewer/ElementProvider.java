/**
 * 
 */
package vmk.docbrowser.rap.docviewer;

import java.util.Collection;

import org.eclipse.rap.rwt.RWT;
import org.osgi.service.log.LogService;

import vmk.docbrowser.common.entity.DocumentDirectory;
import vmk.docbrowser.common.entity.DocumentNode;
import vmk.docbrowser.common.provider.DocumentOperationResult;
import vmk.docbrowser.common.provider.Documents;
import vmk.docbrowser.rap.Context;
import vmk.docbrowser.rap.EventBusUISession;
import vmk.docbrowser.rap.Logger;
import vmk.docbrowser.rap.events.EventDocumentCreated;
import vmk.docbrowser.rap.events.EventDocumentDeleted;
import vmk.docbrowser.rap.events.EventMessage;
import vmk.docbrowser.rap.events.EventMessage.Level;

/**
 * 
 * Document's provider. Executes documents operation, posts events.
 * Is uses as input object for {@code TreeViewer}
 * 
 * @author Maksim Vakhnik
 *
 */
public class ElementProvider {
	private DocumentNode root;

	private ElementProvider() {
		initInstance();
	}

	public static ElementProvider getInstance() {
		Object instance = RWT.getUISession().getAttribute(
				ElementProvider.class.getName());
		if (instance == null) {
			instance = new ElementProvider();
		}
		return (ElementProvider) instance;
	}

	private void initInstance() {
		RWT.getUISession().setAttribute(ElementProvider.class.getName(), this);
	}

	/**
	 * Gets root of documents. 
	 * @return {@code DocumentNode.getRoot}
	 */
	public ElementProvider getRoot() {
		root = DocumentNode.getRoot();
		return this;
	}

	/**
	 * Gets elements without input element.
	 * @param inputElement
	 * @return children of input element.
	 */
	public Object[] getElements(Object inputElement) {

		// first element as root, received first time
		if (inputElement == this) {
			return new Object[] { root };
		}
		return getChildren(inputElement);
	}

	/**
	 *  Gets children without input element.
	 * @param parentElement is {@code DocumentNode}
	 * @return children of input element.
	 */
	public Object[] getChildren(Object parentElement) {
		DocumentNode parent = null;
		if (parentElement != null) {
			parent = (DocumentNode) parentElement;
		}
		Collection<DocumentNode> children = getChildren(parent);
		return children.toArray();
	}

	private Collection<DocumentNode> getChildren(DocumentNode node) {
		Collection<DocumentNode> children = getDocuments().getChidren(node);
		
		return children;
	}

	/**
	 * Returns parent of DocumentNode.
	 * @param element is {@code DocumentNode}
	 * @return
	 */
	public Object getParent(Object element) {
		DocumentNode parent = (DocumentNode) element;

		return parent.getParent();
	}

	public boolean hasChildren(Object element) {
		DocumentNode parent = (DocumentNode) element;
	
		if (!parent.isDirectory()) {
			return false;
		}
		
		if (getChildren(element).length > 0) {
			return true;
		}
		return false;
	}

	private Documents getDocuments() {
		return Context.getDocuments();
	}

	/**
	 * Removes document node from database, posts message EventDocumentDeleted.
	 * 
	 * @param node
	 */
	public void delete(DocumentNode node) {
		try {
		DocumentOperationResult result = getDocuments().delete(node);
		if (result == DocumentOperationResult.OK) {
			EventBusUISession.post(new EventDocumentDeleted(node));
			EventBusUISession.post(new EventMessage(Level.INFO, result
					.getMessage()));
		} else {
			Logger.getInstance().log(LogService.LOG_ERROR, result.getMessage());
			EventBusUISession.post(new EventMessage(Level.ERROR, result
					.getMessage()));
		}
		} catch (Exception e) {
			EventBusUISession.post(new EventMessage(Level.ERROR,
					"Something wrong."));
			Logger.getInstance().log(e);
		}
	}

	/**
	 * Creates directory, posts event if success.
	 * 
	 * @param parent
	 * @param name
	 */
	public void createDirectory(DocumentDirectory parent, String name) {
		try {
		DocumentOperationResult result = getDocuments().createDirectory(parent,
				name);

		if (result == DocumentOperationResult.OK) {
			DocumentNode resDoc = (DocumentNode) result.getResult();
			EventBusUISession.post(new EventDocumentCreated(resDoc));
			EventBusUISession.post(new EventMessage(Level.INFO, result
					.getMessage()));
		} else {
			Logger.getInstance().log(LogService.LOG_ERROR, result.getMessage());
			EventBusUISession.post(new EventMessage(Level.ERROR, result
					.getMessage()));
		} 
		} catch (Exception e) {
			EventBusUISession.post(new EventMessage(Level.ERROR,
					"Something wrong."));
			Logger.getInstance().log(e);
		}

	}

	/**
	 * Creates new document in parent directory, posts event if success.
	 * @param parent
	 * @param name
	 */
	public void createDocument(DocumentDirectory parent, String name) {
		try {
		DocumentOperationResult result = getDocuments().createDocument(parent,
				name);
		if (result == DocumentOperationResult.OK) {
			DocumentNode resDoc = (DocumentNode) result.getResult();
			EventBusUISession.post(new EventDocumentCreated(resDoc));
			EventBusUISession.post(new EventMessage(Level.INFO, result
					.getMessage()));
		} else {
			Logger.getInstance().log(LogService.LOG_ERROR, result.getMessage());
			EventBusUISession.post(new EventMessage(Level.ERROR, result
					.getMessage()));
		} 	} catch (Exception e) {
			EventBusUISession.post(new EventMessage(Level.ERROR,
					"Something wrong."));
			Logger.getInstance().log(e);
		}
		
	}

	/**
	 * Copy source to target recursively. Posts corresponding messages.
	 * @param source
	 * @param target
	 */
	public void copy(DocumentNode source, DocumentDirectory target) {
		try {
		DocumentOperationResult result = getDocuments().copy(source, target);
		if (result == DocumentOperationResult.OK) {
			DocumentNode created = (DocumentNode) result.getResult();
			EventBusUISession.post(new EventMessage(Level.INFO, result
					.getMessage()));
			EventBusUISession.post(new EventDocumentCreated(created));
		} else {
			Logger.getInstance().log(LogService.LOG_ERROR, result.getMessage());
			EventBusUISession.post(new EventMessage(Level.ERROR, result
					.getMessage()));
			
		}
	} catch (Exception e) {
		EventBusUISession.post(new EventMessage(Level.ERROR,
				"Something wrong."));
		Logger.getInstance().log(e);
	}
	}

	/**
	 * Moves source to target, posts events.
	 * @param source
	 * @param target
	 */
	public void move(DocumentNode source, DocumentDirectory target) {

		try {
			DocumentOperationResult result = getDocuments()
					.move(source, target);
			if (result == DocumentOperationResult.OK) {
				DocumentNode created = (DocumentNode) result.getResult();
				EventBusUISession.post(new EventMessage(Level.INFO, result
						.getMessage()));
				EventBusUISession.post(new EventDocumentDeleted(source));
				EventBusUISession.post(new EventDocumentCreated(created));
			} else {
				Logger.getInstance().log(LogService.LOG_ERROR, result.getMessage());
				EventBusUISession.post(new EventMessage(Level.ERROR, result
						.getMessage()));
			}
		} catch (Exception e) {
			String message = "Something wrong.";
			Logger.getInstance().log(e);
			EventBusUISession.post(new EventMessage(Level.ERROR,
					message));
		}

	}

	/**
	 * Renames document. Posts event messages.
	 * @param document
	 * @param name
	 */
	public void rename(DocumentNode document, String name) {
		try {
			DocumentOperationResult result = getDocuments().rename(document,
					name);
			if (result == DocumentOperationResult.OK) {
				EventBusUISession.post(new EventMessage(Level.INFO, result
						.getMessage()));
				EventBusUISession.post(new EventDocumentCreated(document));
			} else {
				Logger.getInstance().log(LogService.LOG_ERROR, result.getMessage());
				EventBusUISession.post(new EventMessage(Level.ERROR, result
						.getMessage()));
			}
		} catch (Exception e) {
			String message = "Something wrong.";
			Logger.getInstance().log(LogService.LOG_ERROR, message);
			EventBusUISession.post(new EventMessage(Level.ERROR,
					message));
			Logger.getInstance().log(e);
		}
	}

}
