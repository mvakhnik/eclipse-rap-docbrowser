/**
 * 
 */
package vmk.docbrowser.rap.docviewer;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import vmk.docbrowser.common.entity.DocumentNode;
import vmk.docbrowser.rap.Resources;

/**
 * Label provider for a TreeViewer. Based on {@code LabelProvider}
 * @author Maksim Vakhnik
 *
 */
public class LabelProviderDocuments implements ILabelProvider{

	/**
	 * 
	 */
	private static final long serialVersionUID = 201501152247L;
	private LabelProvider labelProvider;
	
	
	
	public LabelProviderDocuments() {
		labelProvider = new LabelProvider();
//		initResources();
	}
	

	
	@Override
	public Image getImage(Object element) {
		DocumentNode doc = (DocumentNode) element;
		if (doc.isDirectory()) {
			return Resources.Images.ICON_DIRECTORY.getImage();
		} else {
			return Resources.Images.ICON_DOCUMENT.getImage();
		}
	}


	@Override
	public void addListener(ILabelProviderListener listener) {
		labelProvider.addListener(listener);
	}

	@Override
	public void dispose() {
		labelProvider.dispose();
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return labelProvider.isLabelProperty(element, property);
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
		labelProvider.removeListener(listener);
	}


	@Override
	public String getText(Object element) {
		DocumentNode doc = (DocumentNode) element;
		return doc.getName();
	}

}
