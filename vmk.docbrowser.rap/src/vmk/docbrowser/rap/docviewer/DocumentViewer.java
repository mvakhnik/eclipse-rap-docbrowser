package vmk.docbrowser.rap.docviewer;

import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import vmk.docbrowser.common.entity.DocumentNode;
import vmk.docbrowser.rap.EventBusUISession;
import vmk.docbrowser.rap.events.EventDocumentCreated;
import vmk.docbrowser.rap.events.EventDocumentDeleted;
import vmk.docbrowser.rap.events.EventOpenDocumentNode;
import vmk.docbrowser.rap.events.EventSelectDocumentNode;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;

/**
 * Document browser controller. Refreshes the UI through events from {@code EventBusUISession}
 * 
 * @author Maksim Vakhnik
 *
 */
public class DocumentViewer extends Composite {

	private static final long serialVersionUID = 201412301210L;
	private TreeViewer treeViewer;

	/**
	 * Creates the model and the UI. For building - use build().
	 * 
	 * @param parent
	 * @param style
	 */

	public DocumentViewer(Composite parent, int style) {	
		super(parent, style);
		setLayout(new FillLayout(SWT.VERTICAL));
		treeViewer = new TreeViewer(this, SWT.VIRTUAL | SWT.BORDER);

		addListeners();
		inEventBusRegister();
		initTreeViewer();
	}

	private void inEventBusRegister() {
		EventBusUISession.register(this);
	}

	private void addListeners() {

		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
			
				EventBusUISession.post(
						new EventSelectDocumentNode(getSelectedDocument(event.getSelection())));
			}
		});

		treeViewer.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {
				
				EventBusUISession.post(
						new EventOpenDocumentNode(getSelectedDocument(event.getSelection())));
			}
		});
	}

	private DocumentNode getSelectedDocument(ISelection selection) {
		StructuredSelection structSel = (StructuredSelection) selection;
		DocumentNode document = (DocumentNode) structSel
				.getFirstElement();
		
		return document;
	}

	private void initTreeViewer() {

		treeViewer.setContentProvider(new ContentProviderDocuments(
				ElementProvider.getInstance()));
		treeViewer.setLabelProvider(new LabelProviderDocuments());

		treeViewer.setInput(ElementProvider.getInstance().getRoot());
	}

	public ISelection getDocumentSelection() {
		return this.treeViewer.getSelection();
	}

	/**
	 * Refreshes the UI.
	 * 
	 * @param event
	 */
	@Subscribe
	@AllowConcurrentEvents
	public void onDocumentDeleted(EventDocumentDeleted event) {
		treeViewer.refresh(event.getObject().getParent());
	}

	/**
	 * Refreshes the UI.
	 * 
	 * @param event
	 */
	@Subscribe
	@AllowConcurrentEvents
	public void onDocumentCreated(EventDocumentCreated event) {
		treeViewer.refresh(event.getObject().getParent());
	}
}
