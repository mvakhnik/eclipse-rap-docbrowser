/**
 * 
 */
package vmk.docbrowser.rap;

import org.eclipse.rap.rwt.RWT;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

/**
 * LogService implementation. Get service from bundle context.
 * @author Maksim Vakhnik
 *
 */
public class Logger {
	private LogService log;
	private Logger() {
		initInstance();
	}

	public static Logger getInstance() {
		Object instance = RWT.getUISession().getAttribute(
				Logger.class.getName());
		if (instance == null) {
			instance = new Logger();
		}
		return (Logger) instance;
	}

	private  void getLoggerImplementation() {
		BundleContext bc = FrameworkUtil.getBundle(LogService.class).getBundleContext();
		ServiceReference service = bc.getServiceReference(LogService.class.getName());
		if (service != null) {
			this.log =  (LogService) bc.getService(service);
		}
	}
	
	private void initInstance() {
		RWT.getUISession().setAttribute(Logger.class.getName(), this);
		getLoggerImplementation();
	}
	
	/**
	 * Write log info message.
	 * @param message
	 */
	public void log (String message) {
		log(LogService.LOG_INFO, message);
		
	}
	
	public void log(int logLevel, String message, Throwable exception) {
		if (log != null) {
		log.log(logLevel, message, exception);
		} else {
			System.err.println(message);
			System.err.println(exception);
		}
	}
	
	public void log(Throwable exception) {
		log.log(LogService.LOG_ERROR, exception.getMessage(), exception);
	}
	/**
	 * If LogService is available - log to LogService, else log to System.out(err)
	 * @param logLevel
	 * @param message
	 */
	public  void log(int logLevel, String message) {
		if (log != null) {
			log.log(logLevel, message);
		} else {
			if (logLevel == LogService.LOG_ERROR 
					|| logLevel == LogService.LOG_WARNING) {
				System.err.println(message);
			} else {
				System.out.println(message);
			}
			
			
		}
	}
	
}
