package vmk.docbrowser.em.h2;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import vmk.docbrowser.common.provider.EntityManagerProvider;
import aQute.bnd.annotation.component.Component;
import aQute.bnd.annotation.component.Reference;

/**
 * Set entity manager for H2 database.
 * @author Maksim Vakhnik
 *
 */
@Component()
public class EntityManagerH2 implements EntityManagerProvider{

	private EntityManager em;
	
	@Reference(dynamic=true)
	public void setEmf(EntityManagerFactory emf) {
		this.em = emf.createEntityManager();
		System.out.println("EntityManagerH2:setEMF:" + emf);
	}

	
	public void unsetEmf(EntityManagerFactory emf) {
		this.em = emf.createEntityManager();
		System.out.println("EntityManagerH2:unsetEMF:" + emf);
	}
	
	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}


}
