**README**

The training application, document browser for editing, modifying text documents and their hierarchy.


**Capabilities:**

- creation, editing, modifying the documents (simple text);
- creating, editing, modifying the document's hierarchy, represented by directories;
- documents and document's hierarchy stored in a database (H2 database here);
- client part powered by Eclipse RAP, server part is the Jetty, deployed as bundles in a OSGi (Apache Felix used);

Deployed in OSGi container (Apache Felix) with jetty as server.


**Powered by:**

- java 1.7;
- OSGi implementation: Apache Felix;
- Eclipse framework: RAP, JFace;
- server: jetty;
- database: H2, EclipseLink (Gemini JPA);
- development tools: bndtools, Eclipse Luna
- hosting: openshift.com. 

 You can see working version on http://felix-vmk0test.rhcloud.com/

 Sorry, just developed sources yet, no tests.  