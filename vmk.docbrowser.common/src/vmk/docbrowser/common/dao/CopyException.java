/**
 * 
 */
package vmk.docbrowser.common.dao;

import vmk.docbrowser.common.provider.DocumentOperationResult;

/**
 * Occurs when the document's copying is failed.
 * @author Maksim Vakhnik
 *
 */
public class CopyException extends Exception {

	private static final long serialVersionUID = 201501142039L;
	private DocumentOperationResult result;

	public CopyException(DocumentOperationResult result) {
		super();
		this.result = result;
	}

	public DocumentOperationResult getResult() {
		return result;
	}

}
