/**
 * 
 */
package vmk.docbrowser.common.dao;

import java.util.Collection;

import vmk.docbrowser.common.entity.Document;
import vmk.docbrowser.common.entity.DocumentDirectory;
import vmk.docbrowser.common.entity.DocumentNode;
import vmk.docbrowser.common.provider.DocumentOperationResult;

/**
 * The persistence controller interface of DocumentNode.
 * 
 * @author Maksim Vakhnik
 *
 */
public interface IDocumentNodeDAO {

	/**
	 * Creates the document if it is NOT exist.
	 * For checking use read(DocumentNode)
	 *  If contain children - children will be created too.
	 * @param node, not null
	 * @return created DocumentNode (null if something wrong)
	 */
	DocumentNode create(DocumentNode node);

	/**
	 * Returns the DocumentNode by its id.
	 * @param id
	 * @return DocumentNode, null if cannot find.
	 */
	DocumentNode read(String id);
	
	/**
	 * Gets the DocumentNode.
	 * @param node, not null
	 * @return null if not exist
	 */
	DocumentNode read(DocumentNode node);

	boolean isExist(DocumentNode document);
	
	boolean isExist(String docId);

	/**
	 * Removes the DocumentNode with all children;
	 * @param node - not null child;
	 */
	void remove(DocumentNode node);
	
	/**
	 * Removes the DocumentNode with all children;
	 * @param node - not null child;
	 */
	void remove(String nodeId);

	/**
	 * Updates the documents and all children.
	 * @param node - not null
	 * @return updated DocumentNode, null if something wrong
	 */
	DocumentNode update(DocumentNode node);
	
	/**
	 * Gets the children of the document.
	 * @param node, can be null
	 * @return Collection<DocumentNode> without the parent or empty collection if the document was not found.
	 */
	Collection<DocumentNode> getChildren(DocumentNode parent);

	/**
	 * Reads the data from the document from the defined position.
	 * @param doc
	 * @return null if content not exist.
	 */
	byte[] readContent(Document doc, int start, int length);

	void writeContent(Document doc, int start, byte[] data);

	void writeContent(Document doc, byte[] data);

	/**
	 * Reads the data from the document.
	 * @param doc
	 * @return null if content not exist.
	 */
	byte[] readContent(Document doc);
	
	/**
	 * Copies the source to the target with all children and content.
	 * @param source
	 * @param target
	 * @return {@code DocumentOperationResult} with copied node if success
	 * @throws CopyException
	 */
	DocumentOperationResult copy(DocumentNode source, DocumentDirectory target) throws CopyException;
	
	/**
	 * Renames the  document.
	 * @param document
	 * @param name
	 * @return
	 */
	DocumentOperationResult rename(DocumentNode document, String name);
}
