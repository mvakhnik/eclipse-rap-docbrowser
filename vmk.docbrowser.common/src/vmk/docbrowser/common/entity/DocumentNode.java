/**
 * Document node of tree structure. 
 * Each element contain reference to children and parent.
 * After creation if parent not null add self to parent children. 
 * Compared by name as default.
 */
package vmk.docbrowser.common.entity;

import java.io.Serializable;
import java.util.Comparator;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Transient;

/**
 * Abstract document's class, defines main operations with the documents.
 * @author Maksim Vakhnik
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Access(AccessType.PROPERTY)
public abstract class DocumentNode implements Serializable,
		Comparable<DocumentNode>, IDocumentDirectory {

	/**
	 * Comparator by name.
	 */
	public static final Comparator<DocumentNode> COMPARATOR_BY_NAME = new Comparator<DocumentNode>() {

		@Override
		public int compare(DocumentNode o1, DocumentNode o2) {

			String id1 = o1.getId();
			String id2 = o2.getId();

			return id1.compareTo(id2);
		}

	};

	@Transient
	protected Comparator<DocumentNode> comparatorDefault = COMPARATOR_BY_NAME;
	private static final long serialVersionUID = 201412281121L;

	private String id;
	protected String name;
	protected DocumentNode parent;
	protected long timeCreation;
	protected long timeModification;

	@Transient
	private static final DocumentRoot root;

	/*
	 * DocumentRoot of document's hierarchy.
	 */
	private static class DocumentRoot extends DocumentDirectory {

		private static final long serialVersionUID = 201501291921L;

		private DocumentRoot() {
			super("root");
			this.parent = null;
			this.name = null;
		
		}

		@Override
		public String getId() {
			return null;
		}
		
		@Override
		public boolean isRoot() {
			return true;
		}

	}

	static {
		root = new DocumentRoot();
	}

	{
		this.timeCreation = now();
		comparatorDefault = COMPARATOR_BY_NAME;
	}

	public static DocumentNode getRoot() {
		return root;
	}

	@SuppressWarnings("unused")
	private DocumentNode() {

	}

	/**
	 * DocumentNode constructor.
	 * 
	 * @param name
	 *            - not null.
	 */
	protected DocumentNode(String name) {

		this.name = name;
		validateName(name);
		setTimeModification(getTimeCreation());
	}

	/**
	 * Validates the name. Invoked from the constructor. As default do nothing.
	 * 
	 * @return {@code NameValidationResult}
	 * @throws IllegalArgumentException
	 */
	private NameValidationResult validateName(String name)
			throws IllegalArgumentException {
		if (name == null) {
			throw new IllegalArgumentException(
					"Document cannot be created with name == null."
							+ " Use DocumentNode.getRoot() for root creation.");
		}
		NameValidationResult validateNewName = this.validateNewName(name);
		if (validateNewName != NameValidationResult.OK) {
			throw new IllegalArgumentException(validateNewName.getMessage());
		}
		return this.validateNewName(name);
	}

	/**
	 * Creates the copy of  document.
	 * 
	 * @return DocumentNode copy without parent.
	 */
	public abstract DocumentNode copy();

	/*
	 * Return now time.
	 */
	private long now() {
		return System.nanoTime();
	}

	/**
	 * Generates id from parenId and parent name as "/parentId/name";
	 */
	protected String generateId(DocumentNode parent) {
		String id = getPath();
		return id;
	}

	/**
	 * Gets the id of  DocumentNode.
	 * 
	 * @return null if root.
	 */
	@Id
	public String getId() {
		this.id = this.generateId(parent);
		return id;
	}

	protected void setId(String documentNodeId) {
		this.id = new String(documentNodeId);
	}

	public DocumentNode getParent() {
		return parent;
	}

	public String getName() {
		return name != null ? name : "";
	}

	public void setName(String name) {
		this.name = name;
		setTimeModification(now());
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DocumentNode [documentNodeId=");
		builder.append(getId());
		builder.append(", name=");
		builder.append(name);
		builder.append(", parent=");
		builder.append(parent);
		builder.append(", timeCreation=");
		builder.append(timeCreation);
		builder.append(", timeModification=");
		builder.append(timeModification);
		builder.append("]");
		return builder.toString();
	}

	public long getTimeCreation() {
		return timeCreation;
	}

	void setTimeCreation(long timeCreation) {
		this.timeCreation = timeCreation;
	}

	public long getTimeModification() {
		return timeModification;
	}

	/**
	 * Change time modification.
	 * 
	 * @param timeModification
	 */
	public void setTimeModification(long timeModification) {
		this.timeModification = timeModification;
	}

	/**
	 * Change parent of node.
	 * 
	 * @param parent
	 *            - can be null
	 * @return modified DocumentNode
	 * @throws IllegalArgumentException
	 * @throws NullPointerException
	 * @throws IllegalParentException
	 * @throws NotUniqueIdException
	 */
	public void setParent(DocumentNode parent) {
		if (parent == root) {
			parent = null;
		}
		this.parent = parent;
		setTimeModification();
	}

	/**
	 * Set time modification to now
	 */
	public void setTimeModification() {
		setTimeModification(now());
	}

	/**
	 * Compare for id as default.
	 */
	public int compareTo(DocumentNode compared) {
		return this.comparatorDefault.compare(this, compared);
	}

	@Override
	/**
	 * Hash code depends from the id.
	 */
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		String id = getId();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * Equals by id.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocumentNode other = (DocumentNode) obj;
		String id = getId();
		String otherId = other.getId();
		if (id == null) {
			if (otherId != null)
				return false;
		} else if (!id.equals(otherId))
			return false;
		return true;
	}

	/**
	 * @return null as default.
	 */
	@Override
	public DocumentDirectory getDirectory() {
		return null;
	}

	/**
	 * Gets the path of document.
	 * 
	 * @return {@code String}, null if this is root.
	 */
	public String getPath() {

		String path = "";
		String delim = "/";
		DocumentNode parent = getParent();
		if (parent != null) {
			path = parent.getPath();
		}
		path = path + delim + this.getName();
		return path;
		//
		// return getId();
	}
	
	public boolean isRoot() {
		return false;
	}

	public boolean isFile() {
		return false;
	}
	/**
	 * Validates new name correctness.
	 * 
	 * @param name
	 *            - not null.
	 */
	public abstract NameValidationResult validateNewName(String name);

}
