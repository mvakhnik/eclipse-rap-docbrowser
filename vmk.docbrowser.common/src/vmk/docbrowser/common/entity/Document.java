/**
 * 
 */
package vmk.docbrowser.common.entity;

import javax.persistence.Entity;

/**
 * The leaf in the document's tree hierarchy. 
 * @author Maksim Vakhnik
 *
 */
@Entity
public class Document extends DocumentNode {

	private static final long serialVersionUID = 201412281125L;

	private Document() {
		super("new document");
	}

	/**
	 * Creates the new document with no parents.
	 * @param name, not null.
	 */
	public Document(String name) {
		super(name);
	}

	@Override
	public boolean isDirectory() {
		return false;
	}

	@Override
	public NameValidationResult validateNewName(String name){
		NameValidationResult result = DocumentDirectoryNameValidator.validate(name);
		return result;
	}

	/**
	 * Returns the document's copy.
	 */
	@Override
	public DocumentNode copy() {
		DocumentNode copy = new Document(this.getName());
		copy.setTimeCreation(this.timeCreation);
		copy.setTimeModification(this.timeModification);
		return copy;
	}

	@Override
	public boolean isFile() {
		return true;
	}

	
	
}
