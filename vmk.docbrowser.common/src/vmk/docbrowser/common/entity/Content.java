/**
 * 
 */
package vmk.docbrowser.common.entity;

import java.io.Serializable;
import java.util.Arrays;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 * The content of document. Founds by id (equals to document id).
 * @author Maksim Vakhnik
 *
 */
@Entity
public class Content implements Serializable{
	
	private static final long serialVersionUID =201412281123L;
	@Id
	private String contentId;
	private byte[] data;
	@Transient
	private int maxSize = Integer.MAX_VALUE / 100;
		
	@SuppressWarnings("unused")
	private Content() {

	}
	
	/**
	 * Stores the data of the document.
	 * @param owner - {@code Document}
	 */
	public Content(Document owner) {
		this.contentId = owner.getId();
	}
	
	public Content createCopy(Document owner) {
		Content copy = new Content(owner);
		copy.data = Arrays.copyOf(this.data, this.data.length);
		return copy;
	}
	
	/**
	 * Writes the data in the position (overrides the old data). 
	 * @param start
	 * @param data
	 */
	public void write(int start, byte[] data)  {
		int end = start + data.length < Integer.MAX_VALUE ? start + data.length : Integer.MAX_VALUE;
		if (this.data.length < end) {
			this.data = Arrays.copyOf(this.data, end);
		}
		System.arraycopy(data, 0, this.data, start, end);
	}

	/**
	 * Reads the data.
	 * @param start
	 * @param length
	 * @return byte[]
	 */
	public byte[] read(int start, int length) {
		
		int end = start + length < Integer.MAX_VALUE ? 
				start + length : Integer.MAX_VALUE;
		end = end < this.data.length ? end : this.data.length;
		byte[] result = new byte[end - start];
		System.arraycopy(this.data, start, result, 0, end);
		return result;
	}
	
	/**
	 * Gets the size of the current data.
	 * @return
	 */
	public long getSize() {
			return data.length;
	}

	/**
	 * Gets the id.
	 * @return
	 */
	public String getContentId() {
		return contentId;
	}

	/**
	 * Overrides the all data.
	 * @param data
	 */
	public void setData(byte[] data) {
		this.data = data;
	}
	
	public byte[] getData() {
		return data;
	}
	
}
