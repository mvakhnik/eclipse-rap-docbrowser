/**
 * 
 */
package vmk.docbrowser.common.entity;

import javax.persistence.Entity;

/**
 * The document's node in tree hierarchy, can contain (or not) a children. For root 
 * @author Maksim Vakhnik
 *
 */
@Entity
public class DocumentDirectory extends DocumentNode {
	
	private static final long serialVersionUID = 201412281125L;

	@SuppressWarnings("unused")
	private DocumentDirectory() {
		this("default");
	}

	/**
	 * Create DocumentDirectory with default parent == null.
	 * 
	 * @param name
	 *            not null, not empty
	 */
	public DocumentDirectory(String name) {
		super(name);
	}

	@Override
	public DocumentDirectory getDirectory() {
		return this;
	}

	
	
	@Override
	public boolean isDirectory() {
		return true;
	}

	@Override
	public  NameValidationResult validateNewName(String name) {
			NameValidationResult result = DocumentDirectoryNameValidator.validate(name);
			return result;
	}

	@Override
	public DocumentNode copy() {
			DocumentNode copy = new DocumentDirectory(this.getName());
			copy.setTimeCreation(this.timeCreation);
			copy.setTimeModification(this.timeModification);
			return copy;
	}

//	/**
//	 * Change getId for root.
//	 * @return null if root
//	 */
//	@Override
//	public String getId() {
//		String id = null;
		
//		if (this.name == null) {
//			
//			return null;
//		}
//		return super.getId();
//	}

}
