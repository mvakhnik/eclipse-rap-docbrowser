/**
 * 
 */
package vmk.docbrowser.common.entity;

/**
 * Result of name validation.
 * @author Maksim Vakhnik
 *
 */
public enum NameValidationResult {
	
	OK("Name is fine."),
	ERROR_EMPTY_NAME("Empty name is not correct."),
	ERROR_NULL_NAME("Null name is not correct.."),
	ERROR_WRONG_NAME("Name is not correct: contains not correct symbols [" 
			+ DocumentNameValidator.getForbiddenSymbols() + "]'");
	
	private final String message;
	
	private NameValidationResult(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
	
	
}
