/**
 * 
 */
package vmk.docbrowser.common.entity;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Validate document name.
 * @author Maksim Vakhnik
 *
 */
public class DocumentNameValidator {
	

	
	private static final Set<String> forbiddenSymbols;
	static {
		forbiddenSymbols = new HashSet<>();
		forbiddenSymbols.add("/");
	}
	
	public static NameValidationResult validate(String name) {
		if (name == null) {
			return NameValidationResult.ERROR_NULL_NAME;
		}
		if (name.isEmpty()) {
			return NameValidationResult.ERROR_EMPTY_NAME;
		}
		if (isWrong(name)) {
			return NameValidationResult.ERROR_WRONG_NAME;
		}
		return NameValidationResult.OK;
	}
	
	/**
	 * Validate name for default constraints.
	 * @param name not null
	 * @return true if name not correct
	 */
	protected static boolean isWrong(String name) {
		for (String s : forbiddenSymbols) {
			if (name.contains(s)) return true;
		}
		return false;
	}

	/**
	 * Forbidden symbols.
	 * @return Collection<String>
	 */
	protected static Collection<String> getForbiddenSymbols() {
		return forbiddenSymbols;
	}

}
