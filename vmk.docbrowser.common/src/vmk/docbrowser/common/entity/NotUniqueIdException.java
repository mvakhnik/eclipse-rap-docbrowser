/**
 * 
 */
package vmk.docbrowser.common.entity;

import java.io.IOException;

/**
 * Exception occurs when DocumentNode child inserted in parent with same child.
 * @author Maksim Vakhnik
 *
 */
public class NotUniqueIdException extends IOException{

	private static final long serialVersionUID = 201501091035L;

	public NotUniqueIdException() {
		super();
	}

	public NotUniqueIdException(String message, Throwable cause) {
		super(message, cause);
	}

	public NotUniqueIdException(String message) {
		super(message);
	}

	public NotUniqueIdException(Throwable cause) {
		super(cause);
	}
	
}
