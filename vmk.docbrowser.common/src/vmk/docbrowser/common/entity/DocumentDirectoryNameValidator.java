/**
 * 
 */
package vmk.docbrowser.common.entity;

/**
 * @author Maksim Vakhnik
 *
 */
public class DocumentDirectoryNameValidator extends DocumentNameValidator {

	public static NameValidationResult validate(String name) {
		if (name != null) {
			if (name.isEmpty()) {
				return NameValidationResult.ERROR_EMPTY_NAME;
			}
			if (isWrong(name)) {
				return NameValidationResult.ERROR_WRONG_NAME;
			}
		}
		return NameValidationResult.OK;
	}
}
