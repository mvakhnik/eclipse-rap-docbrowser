/**
 * 
 */
package vmk.docbrowser.common.entity;

/**
 * @author Maksim Vakhnik
 *
 */
public interface IDocumentDirectory {

	/**
	 * Get DocumentDirectory.
	 * @return DocumentDirectory, can be null.
	 */
	DocumentDirectory getDirectory();
	
	/**
	 * 
	 * @return true if is directory
	 */
	boolean isDirectory();
}
