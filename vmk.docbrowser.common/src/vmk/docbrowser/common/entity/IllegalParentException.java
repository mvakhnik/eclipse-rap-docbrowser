/**
 * 
 */
package vmk.docbrowser.common.entity;

import java.io.IOException;

/**
 * Occurs when parent changed and new parent is not correct:
 *  <li>
 *  new parent is child  of modified node{@code DocumentNode};
 *  new parent equals modified parent
 *  new parent same as old parent
 *  </li>
 * @author Maksim Vakhnik
 *
 */
public class IllegalParentException extends IOException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 201501021745L;

	public IllegalParentException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public IllegalParentException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public IllegalParentException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public IllegalParentException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
