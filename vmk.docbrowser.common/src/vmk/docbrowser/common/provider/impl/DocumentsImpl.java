/**
 * Consists methods for DocumentNodes manipulations.
 */
package vmk.docbrowser.common.provider.impl;

import java.util.Collection;

import org.osgi.service.log.LogService;

import vmk.docbrowser.common.dao.CopyException;
import vmk.docbrowser.common.dao.IDocumentNodeDAO;
import vmk.docbrowser.common.entity.Document;
import vmk.docbrowser.common.entity.DocumentDirectory;
import vmk.docbrowser.common.entity.DocumentNode;
import vmk.docbrowser.common.entity.NameValidationResult;
import vmk.docbrowser.common.provider.DocumentOperationResult;
import vmk.docbrowser.common.provider.Documents;
import aQute.bnd.annotation.component.Activate;
import aQute.bnd.annotation.component.Component;
import aQute.bnd.annotation.component.Reference;

//
/**
 * @author Maksim Vakhnik
 *
 */
@Component
public class DocumentsImpl implements Documents {

	private IDocumentNodeDAO dao;

	private LogService log;
	
	@Reference(optional = true, dynamic = true)
	private void setLog(LogService log) {
		this.log = log;
	}
	
	@SuppressWarnings("unused")
	private void unsetLog(LogService log) {
		this.log = null;
	}
	
	/**
	 * Write log info message.
	 * @param message
	 */
	public void log (String message) {
		log(LogService.LOG_INFO, message);
	}
	
	/**
	 * If LogService is available - log to LogService, else log to System.out(err)
	 * @param logLevel
	 * @param message
	 */
	private  void log(int logLevel, String message) {
		if (log != null) {
			log.log(logLevel, message);
		} else {
			if (logLevel == LogService.LOG_ERROR 
					|| logLevel == LogService.LOG_WARNING) {
				System.err.println(message);
			} else {
				System.out.println(message);
			}
		}
	}

	@Activate
	public void init() {
		log("activate:" + this);
	}

	@Reference(dynamic = true)
	public void setDao(IDocumentNodeDAO dao) {
		log("set dao:" + dao);
		this.dao = dao;
	}

	public void unsetDao(IDocumentNodeDAO dao) {
		log("unset dao:" + dao);
		this.dao = dao;
	}

	@Override
	public Collection<DocumentNode> getChidren(String path) {
		String currentPath = (path == null || path.isEmpty() || path
				.equals("/")) ? null : path;
		DocumentNode parent = new DocumentDirectory(currentPath);
		return dao.getChildren(parent);
	}

	@Override
	public Collection<DocumentNode> getChidren(DocumentNode parent) {
		return dao.getChildren(parent);
	}

	@Override
	public DocumentNode getDocument(String path) {

		return dao.read(path);
	}

	@Override
	public DocumentOperationResult createDirectory(DocumentDirectory parent,
			String name) {
		DocumentNode node = new DocumentDirectory(name);
		return createDocumentNode(parent, node);
	}

	@Override
	public DocumentOperationResult createDocument(DocumentDirectory parent,
			String name) {
		DocumentNode node = new Document(name);

		return createDocumentNode(parent, node);

	}

	@Override
	public DocumentOperationResult createDocumentNode(DocumentDirectory parent,
			DocumentNode doc) {
		doc.setParent(parent);
		if (dao.isExist(doc)) {
			return DocumentOperationResult.ERROR_DOCUMENT_EXIST.setResult(doc);
		}
		if (dao.create(doc) != null) {
			return DocumentOperationResult.OK.setResult(doc);
		}
		return DocumentOperationResult.ERROR.setResult(doc);
	}

	@Override
	public DocumentOperationResult delete(DocumentNode document) {
		if (document == null) {
			return DocumentOperationResult.ERROR_DOCUMENT_NOT_EXIST;
		}
		dao.remove(document);
		return DocumentOperationResult.OK;
	}

	@Override
	public DocumentOperationResult createUpdateDocument(Document document) {
		if (dao.isExist(document)) {
			DocumentNode result = dao.update(document);
			if (result != null) {
				return DocumentOperationResult.OK.setResult(result);
			} else {
				return DocumentOperationResult.ERROR;
			}
		} else {
			DocumentNode result = dao.create(document);
			if (result != null) {
				return DocumentOperationResult.OK.setResult(result);
			} else {
				return DocumentOperationResult.ERROR;
			}
		}
	}

	@Override
	public DocumentOperationResult readFrom(Document doc, int start, int length) {
		DocumentNode finded = dao.read(doc);
		if (finded != null && !finded.isDirectory()) {
			byte[] data = dao.readContent((Document) finded, start, length);
			return DocumentOperationResult.OK.setResult(data);
		}
		return DocumentOperationResult.ERROR_DOCUMENT_NOT_EXIST;
	}

	@Override
	public DocumentOperationResult writeTo(Document doc, int start, byte[] data) {
		DocumentNode finded = dao.read(doc);
		if (finded != null && !finded.isDirectory()) {
			dao.writeContent((Document) finded, start, data);
			return DocumentOperationResult.OK;
		}
		return DocumentOperationResult.ERROR_DOCUMENT_NOT_EXIST;
	}

	@Override
	public DocumentOperationResult writeTo(Document doc, byte[] data) {
		DocumentNode finded = dao.read(doc);
		if (finded != null && !finded.isDirectory()) {
			dao.writeContent((Document) finded, data);
			return DocumentOperationResult.OK;
		}
		return DocumentOperationResult.ERROR_DOCUMENT_NOT_EXIST;
	}

	@Override
	public DocumentOperationResult readFrom(Document doc) {
		DocumentNode finded = dao.read(doc);
		if (finded != null && !finded.isDirectory()) {
			byte[] data = dao.readContent((Document) finded);
			return DocumentOperationResult.OK.setResult(data);
		}
		return DocumentOperationResult.ERROR_DOCUMENT_NOT_EXIST;
	}

	@Override
	public DocumentOperationResult move(DocumentNode source,
			DocumentDirectory target) {
		DocumentOperationResult result = copy(source, target);
		if (result == DocumentOperationResult.OK) {
			setParentTimeModification(source);
			dao.remove(source);
		}
		return result;
	}

	private void setParentTimeModification(DocumentNode child) {
		DocumentNode parent = child.getParent();
		if (parent != null) {
			parent.setTimeModification();
		}

	}

	/**
	 * Create copy recursively. Content copy too.
	 */
	@Override
	public DocumentOperationResult copy(DocumentNode source,
			DocumentDirectory target) {
		DocumentNode copy = source.copy();
		copy.setParent(target);
		if (dao.isExist(copy)) {
			return DocumentOperationResult.ERROR_DOCUMENT_EXIST
					.setResult(source);
		} else {
			try {
				return dao.copy(source, target);
			} catch (CopyException e) {
				return e.getResult();
			}

		}

	}

	@Override
	public boolean isExist(String path) {
		return dao.isExist(path);
	}

	@Override
	public DocumentOperationResult rename(DocumentNode document, String name) {

		if (document.validateNewName(name) != NameValidationResult.OK) {
			return DocumentOperationResult.ERROR_WRONG_NAME;
		} else {
			return dao.rename(document, name);
		}

	}


}
