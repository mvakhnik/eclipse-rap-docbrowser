/**
 * 
 */
package vmk.docbrowser.common.provider;

import javax.persistence.EntityManager;

/**
 * @author Maksim Vakhnik
 *
 */
public interface EntityManagerProvider {
	public EntityManager getEntityManager();
}
