package vmk.docbrowser.common.provider;

/**
 * Result of CRUD operations.
 * 
 * @author Maksim Vakhnik
 *
 */
public enum DocumentOperationResult {
	OK("Success"),
	ERROR("Error"),
	ERROR_PARENT_DIRECTORY_NOT_EXIST("Error: parent directory not exist"),
	ERROR_PARENT_DIRECTORY_WRONG_NAME("Error: wrong name of parent directory"),
	ERROR_DIRECTORY_EXIST("Error: directory is exists already"),
	ERROR_DIRECTORY_WRONG_NAME("Error: wrong name of directory"), 
	ERROR_DIRECTORY_NOT_EXIST(	"Error: directory is not exist"),
	ERROR_DOCUMENT_NOT_EXIST("Error: document is not exist"),
	ERROR_DOCUMENT_EXIST("Error: document is exists already"),
	ERROR_COPY_PROCESS_FAILED("Error: copy process is failed"),
	ERROR_TARGET_IS_NOT_DIRECTORY("Error: target is not directory"),
	ERROR_TARGET_IS_CHILD_OF_SOURCE("Error: target is child of source"),
	ERROR_SOURCE_IS_TARGET("Error: source is equals to target"),
	ERROR_WRONG_NAME("Error: wrong name");

	private Object result;
	private String message;

	private DocumentOperationResult(String message) {
		this.message = message;
	}

	public Object getResult() {
		return result;
	}

	public DocumentOperationResult setResult(Object result) {
		this.result = result;
		return this;
	}

	public String getMessage() {
		return message;
	}

	public DocumentOperationResult setMessage(String message) {
		this.message = message;
		return this;
	}

}