/**
 * Consists methods for DocumentNodes manipulations.
 * 
 */
package vmk.docbrowser.common.provider;

import java.util.Collection;

import vmk.docbrowser.common.entity.Document;
import vmk.docbrowser.common.entity.DocumentDirectory;
import vmk.docbrowser.common.entity.DocumentNode;

/**
 * 
 * Provides operations with {@code DocumentNode} 
 * @author Maksim Vakhnik
 *
 */
public interface Documents {

	/**
	 * Returns the document's collection by this path.
	 * 
	 * @param path
	 *            , if null, "" or "/" - return root
	 * @return Collection<DocumentNode>, if documents not founded - return empty
	 *         collection.
	 */
	Collection<DocumentNode> getChidren(String pathParent);

	/**
	 * Returns the document's children.
	 * 
	 * @param if null - return root
	 * @return Collection<DocumentNode>, if documents not founded - return empty
	 *         collection.
	 */
	Collection<DocumentNode> getChidren(DocumentNode parent);

	/**
	 * Gets the document by path.
	 * 
	 * @param path
	 *            == DocumentNode.id;
	 * @return null if not successfully.
	 */
	DocumentNode getDocument(String path);

	/**
	 * Creates the new directory.
	 * 
	 * @param path
	 *            - path of parent document directory.
	 * @param name
	 *            - name of new directory.
	 * @return {@code Documents.Result}
	 */
	DocumentOperationResult createDirectory(DocumentDirectory parent, String name);

	/**
	 * Deletes the {@code DocumentNode} with children.
	 * 
	 * @param document
	 *            for deleting
	 * @return Result, when Result.getResult() is null.
	 */
	DocumentOperationResult delete(DocumentNode document);

	/**
	 * Creates new Document in parent directory.
	 * 
	 * @param parent
	 * @param name
	 * @return {@code Result.OK} - if success
	 */
	DocumentOperationResult createDocument(DocumentDirectory parent, String name);

	/**
	 * Creates or updates the document.
	 * 
	 * @param document
	 * @return
	 */
	DocumentOperationResult createUpdateDocument(Document document);

	/**
	 * Reads data from defined position.
	 * 
	 * @param doc
	 * @param start
	 *            - posititon for reading.
	 * @param length
	 *            of data
	 * @return {@code Result} where {@code Result.getResult()} == byte[] with
	 *         data
	 */
	DocumentOperationResult readFrom(Document doc, int start, int length);

	/**
	 * Writes a data to the document in defined position. If the data exist - overrides
	 * this data. Not existed - appends.
	 * 
	 * @param doc
	 * @param start
	 *            - posititon for writing.
	 * @param data
	 *            - byte[] for writing
	 * @return {@code Result} where {@code Result.getResult()} == null
	 */
	DocumentOperationResult writeTo(Document doc, int start, byte[] data);

	/**
	 * Writes data content to the document, old content will be overrides.
	 * 
	 * @param doc
	 *            - not null
	 * @param data
	 *            - not null data
	 * @return {@code Result} where {@code Result.getResult()} == null
	 */
	DocumentOperationResult writeTo(Document doc, byte[] data);

	/**
	 * Reads all data from the document;
	 * 
	 * @param doc
	 * @return @return {@code Result} where {@code Result.getResult()} == byte[]
	 *         with data.
	 */
	DocumentOperationResult readFrom(Document doc);

	/**
	 * Creates the document in the directory.
	 * 
	 * @param parent
	 *            - can be null
	 * @param doc
	 *            - not null document or directory.
	 * @return {@code Result} where {@code Result.getResult()} is created
	 *         document
	 */
	DocumentOperationResult createDocumentNode(DocumentDirectory parent, DocumentNode doc);

	/**
	 * Moves the DocumentNode to the new directory with children (if exists).
	 * @param source
	 *            - not null document or directory.
	 * @param target
	 *            - can be null
	 * @return {@code Result} where {@code Result.getResult()} is modified (or not modified if failed)
	 *         document
	 */
	DocumentOperationResult move(DocumentNode source, DocumentDirectory target);


	/**
	 * Copies the document to the new directory with the children (if exists).
	 * @param source
	 *            - not null document or directory.
	 * @param target
	 *            - can be null
	 * @return {@code Result} where {@code Result.getResult()} is modified (or not modified if failed)
	 *         document
	 */
	DocumentOperationResult copy(DocumentNode source, DocumentDirectory target);
	
	/**
	 * Checks the document's existing.
	 * @param path
	 * @return
	 */
	boolean isExist(String path);

	/**
	 * Renames the document.
	 * @param document - not null, not root.
	 * @param name - correct name.
	 * @return
	 */
	DocumentOperationResult rename(DocumentNode document, String name);
}
