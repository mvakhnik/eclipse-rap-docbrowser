package vmk.docbrowser.common.entity;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DocumentNodeTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testRootName() {
		DocumentNode root = new DocumentDirectory(null);
		System.out.println("ROOT:" + root);
		assertEquals("/", root.getId());
	}
	
	@Test
	public void testChangeParent() {
		DocumentNode parent0 = new DocumentDirectory("parent-0");
		assertNotNull(parent0);
		
		DocumentNode child0 = new Document("child");
		assertNotNull(child0);
		
		assertNull(parent0.getParent());
		assertNull(child0.getParent());
		 
		//try set document as parent
			parent0.setParent(child0);
		assertNull(parent0.getParent());
		
		//set new parent in child
			child0.setParent(parent0);
		assertEquals(parent0, child0.getParent());
		
		//add new parent
		DocumentNode parent01 = new DocumentDirectory("parent-01");
			parent0.setParent(parent01);
		
		assertEquals(parent01, parent0.getParent());
		{
			//try set as parent in parent
				parent01.setParent(parent0);
			assertEquals(parent01, parent0.getParent());
		}

		
		//try set document as parent
		DocumentNode oldParent = parent01.getParent();
			parent01.setParent(child0);
		assertEquals(oldParent, parent01.getParent());
		
		//set new parent in child
			child0.setParent(parent01);
		assertEquals(parent01, child0.getParent());
		
		//set new null parent in child
			child0.setParent(null);
		assertEquals(null, child0.getParent());
		
		assertNull(parent01.getParent());
		assertEquals(parent01, parent0.getParent());
		
		//set new null parent in node
			parent0.setParent(null);
		assertEquals(null, child0.getParent());
	
		//try repeat set new null parent in child
			parent0.setParent(null);
		assertEquals(null, child0.getParent());
		
		System.out.println(parent01);
		System.out.println(parent0);
		System.out.println(child0);
	}

}
