package vmk.docbrowser.common.entity;

import static org.junit.Assert.*;

import org.junit.Test;

public class DocumentTest {

	@Test
	public void testNameValidator() {
		DocumentNode nullNameNode = null;
		try {
			nullNameNode = new Document(null);
		} catch ( IllegalArgumentException e) {
			
		}
		assertNull(nullNameNode);
		System.out.println(nullNameNode);
		
		DocumentNode emptyNameNode = null;
		try {
			emptyNameNode = new Document("");
		} catch ( IllegalArgumentException e) {
			
		}
		System.out.println(emptyNameNode);
		assertNull(emptyNameNode);
		
		
		
		DocumentNode wrongNameNode = null;
		try {
			wrongNameNode = new Document("test/node with wrong name");
		} catch ( IllegalArgumentException e) {
			
		}
		System.out.println(wrongNameNode);
		assertNull(wrongNameNode);
	}

}
