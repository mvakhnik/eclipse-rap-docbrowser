package vmk.docbrowser.common.entity;

import static org.junit.Assert.*;

import org.junit.Test;

public class DocumentDirectoryTest {

	@Test
	public void testValidateName() {
		
		DocumentNode nullNameNode = new DocumentDirectory(null);
		assertNotNull(nullNameNode);
		
		System.out.println(nullNameNode);
		
		DocumentNode emptyNameNode = null;
		try {
			emptyNameNode = new DocumentDirectory("");
		} catch ( IllegalArgumentException e) {
			
		}
		System.out.println(emptyNameNode);
		assertNull(emptyNameNode);
		
		
		
		DocumentNode wrongNameNode = null;
		try {
			wrongNameNode = new DocumentDirectory("test/node with wrong name");
		} catch ( IllegalArgumentException e) {
			
		}
		System.out.println(wrongNameNode);
		assertNull(wrongNameNode);
		
	}

}
